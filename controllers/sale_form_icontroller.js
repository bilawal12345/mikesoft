/**
 * Created by belawal on 6/25/19.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');


router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`SELECT sale_dollar.*,parties.name as p_name,accounts.NAME as b_name FROM sale_dollar LEFT JOIN parties on sale_dollar.party_id=parties.PARTY_ID  LEFT JOIN accounts on sale_dollar.account_id=accounts.ACCOUNT_ID `,(err,rows)=>{
        response.render('./sale_form_i/list.twig',{sales:rows});
//        response.send(rows);
    });


});

router.get('/add',(request,res)=>{
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var sale_dollars=`select * from sale_dollar ORDER BY id DESC limit 1`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_dollars, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.sale_dollars = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);

        if(return_data.sale_dollars.length>0){
            var the_form_i_number=return_data.sale_dollars[0].form_i_number+1;

        }
        else{
            the_form_i_number=the_form_i_number+1;
        }
//        console.log(the_form_i_number);
        res.render('./sale_form_i/add.twig',{'customers':return_data.customers,'form_i':the_form_i_number });
    });
});

router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var party_id="";
    var data={
       'form_i_number':request.body.form_i_number,
        date:request.body.date,
        time:request.body.time,
        party_id:request.body.party_id,
        amount:request.body.amount,
        per_dollar_rate:request.body.per_dollar_rate,
        type:'FORM'
    }
    mysqlconnection.query('INSERT INTO sale_dollar set ?',data,(err,rows)=>{

    });

    var data={
        voucher_no:request.body.form_i_number,
        voucher_date:request.body.date,
        type:"SALE_FORM",
        party_id:request.body.party_id,
        credit:0,
        COMPANY_ID:company_id,
        debit:request.body.amount,
        remarks:'it is sale Form I'
    }
    mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
        if(err) console.log(err)

    });

    response.redirect('/sale_form_i/list');



});

module.exports=router;