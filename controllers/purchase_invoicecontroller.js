/**
 * Created by belawal on 2/13/19.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select purchase_master.*,parties.name as p_name from purchase_master LEFT JOIN parties on purchase_master.customer_id=parties.PARTY_ID where purchase_master.type='P' AND purchase_master.company_id=${company_id}`,(err,rows)=>{
        if(err) response.send(err);

        response.render('./purchase_invoice/list.twig',{purchases:rows});
    });



});

router.get('/add',(request,res)=>{
    var purchase_id = request.session.purchase_id;
    request.session.purchase_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='V'`;
    var categories = `select * from category where COMPANY_ID=${company_id}`;
    var sale_master=`select * from purchase_master where type='P' ORDER BY ID DESC limit 1`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(categories, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.invoice = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        if(return_data.invoice.length>0){
            var invoice_number=return_data.invoice[0].invoice_number+1;
        }
        else{
            var invoice_number=1;
        }
        res.render('./purchase_invoice/add.twig',{'customers':return_data.customers , 'categories':return_data.categories,'invoice_number':invoice_number,purchase_id:purchase_id});
    });
});

router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        invoice_number:request.body.invoice_number,
        invoice_date:request.body.invoice_date,
        payment_type:request.body.payment_type,
        customer_id:request.body.customer_id,
        freight:request.body.freight,
        gross_amount:request.body.gross_amount,
        discount:request.body.discount,
        net_amount:request.body.net_amount,
        internal_notes:request.body.internal_notes,
        external_notes:request.body.external_notes,
        type:'P',
        company_id:company_id
    }
    mysqlconnection.query('insert into purchase_master set ?',data,(err,rows)=>{
        if(!err){
            var purchase_master_id=rows.insertId;
            var categories=request.body.category_id;
            var product_id=request.body.product_id;
            var quantity=request.body.quantity;
            var total=request.body.total;

            var weight=request.body.weight;
            var cost_price=request.body.cost_price;
            for(var i=0;i<categories.length;i++){
                var data={
                    purchase_master_id:purchase_master_id,
                    category_id:categories[i],
                    product_id:product_id[i],
                    quantity:quantity[i],
                    weight:weight[i],
                    total:total[i],
                    cost_price:cost_price[i]
                }
                mysqlconnection.query('insert into purchase_child set ?',data,(err,rows)=>{

                });
            }


            for(var i=0;i<categories.length;i++){
                var data={
                    transection_id:request.body.invoice_number,
                    transection_date:request.body.invoice_date,
                    company_id:company_id,
                    category_id:categories[i],
                    product_id:product_id[i],
                    quantity:quantity[i],
                    cost_price:cost_price[i],
                    type:'PURCHASE'
                }
                mysqlconnection.query('insert into product_ledger set ?',data,(err,rows)=>{
                  if(err) console.log(err);
                });
            }

            console.log("the master id is " +rows.insertId);
            request.session.purchase_id=rows.insertId;


            if(request.body.payment_type == "credit" && request.body.customer_id != "") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"PURCHASE",
                    party_id:request.body.customer_id,
                    credit:request.body.net_amount,
                    debit:0,
                    COMPANY_ID:company_id,
                    remarks:'Credit Purchase'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }

            if (request.body.payment_type == "cash") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"PURCHASE",
                    COMPANY_ID:company_id,
                    credit:request.body.net_amount,
                    debit:0,
                    remarks:'PURCHASE  on invoice number'+request.body.invoice_number + " on party id " + request.body.customer_id
                }
                mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }


            response.redirect('/purchase_invoice/add');
        }
        else{
            response.send(err)
        }
    });
});


router.get('/print/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
    var sale_master=`select purchase_master.*,parties.NAME as party_name,parties.CONTACT_NO,parties.EMAIL,parties.ADDRESS from purchase_master LEFT JOIN parties on parties.PARTY_ID=purchase_master.customer_id where purchase_master.company_id=${company_id} And id=${id}`;
    var sale_child=`select purchase_child.*,products.NAME as product_name from purchase_child  LEFT JOIN products on products.PRODUCT_ID=purchase_child.product_id   where  purchase_child.purchase_master_id=${id}`;
    var return_data = {};



    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.master = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_child, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.child = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

        response.render('./purchase_invoice/print2.twig',{'masters':return_data.master , 'childs':return_data.child,session:session_values});
    });


});


router.get('/cat_product/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from products where COMPANY_ID=${company_id} AND CATEGORY_ID=${request.params.id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});

router.get('/get_customer_detail/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from parties where COMPANY_ID=${company_id} AND PARTY_ID=${request.params.id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});


module.exports=router;