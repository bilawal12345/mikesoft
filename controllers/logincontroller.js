/**
 * Created by belawal on 1/15/19.
 */

const express=require('express');
var cookieParser = require('cookie-parser');

const app=express();

var router=express.Router();

const bodyparser=require('body-parser');
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());


app.use(cookieParser());

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

var mysqlconnection=require('../connection.js');

app.use(function (req, res, next) {
    res.locals = {
        site: {
            title: 'ExpressBootstrapEJS',
            description: 'A boilerplate for a simple web application with a Node.JS and Express backend, with an EJS template with using Twitter Bootstrap.'
        },
        author: {
            name: 'Cory Gross',
            contact: 'CoryG89@gmail.com'
        }
    };
    next();
});

app.set('views','./views');
app.set('view engine','twig');


router.get("/malikice",(request,res)=>{
    request.session.name='Malik ICE';
    request.session.COMPANY_ID=31;
    request.session.CONTACT=03369189596;
    request.session.EMAIL='icetry@gmail.com';
    request.session.SHORT_NAME='Malik_Ice';
    request.session.LOGO_LARGE='4-MIF.png';
    res.render('./login/userlogin2.twig',{image: request.session.LOGO_LARGE,session: request.session})

//    res.render('./login/companycode.twig')
});

router.get("/",(request,res)=>{
//     request.session.name='Malik ICE';
// //    request.session.COMPANY_ID=31;
//     request.session.CONTACT=03369189596;
//     request.session.EMAIL='icetry@gmail.com';
//     request.session.SHORT_NAME='Malik_Ice';
//    request.session.LOGO_LARGE=result[0].LOGO_LARGE;
//     res.render('./login/userlogin2.twig',{image: request.session.LOGO_LARGE,session: request.session})

    res.render('./login/companycode.twig')
});

router.get("/adminlogin",(request,res)=>{
    res.render('./login/adminlogin.twig')
});

router.post('/adminlogin',(request,response)=>{
    var email=request.body.email;
    var password=request.body.password;

    mysqlconnection.query('select * from admins where EMAIL=? and password=?',[email,password],(err,results)=>{
        if(results.length>0){
            request.session.COMPANY_ID=1;
            request.session.admin_email=results[0].email;
            response.redirect('company/add');
        }
        else{

            response.render('./login/adminlogin.twig',{msg:'Email or Password is wrong'})
        }
    });
});



router.post('/companycode',(request,response)=>{
   var shortname=request.body.code;
   mysqlconnection.query('SELECT * FROM company_setup WHERE SHORT_NAME=?',[shortname],(err,result)=>{
      if(result.length>0){

          request.session.name=result[0].NAME;
          request.session.COMPANY_ID=result[0].COMPANY_ID;
          request.session.CONTACT=result[0].CONTACT;
          request.session.EMAIL=result[0].EMAIL;
          request.session.SHORT_NAME=result[0].SHORT_NAME;
          request.session.LOGO_LARGE=result[0].LOGO_LARGE;
             request.session.tax_number=result[0].tax_number;
          return response.render('./login/userlogin2.twig',{image: request.session.LOGO_LARGE,session: request.session})
      }
      else{
          response.render('./login/companycode.twig',{msg:'Company Code is wrong'})
      }
   })
});


router.post('/userlogin',(request,response)=>{
   var email=request.body.email;
   var password=request.body.password;
   var company_id=request.session.COMPANY_ID;
   var role=request.body.role;
  console.log(company_id);

// response.send(company_id);

  mysqlconnection.query('select * from parties where EMAIL=? and password=? and COMPANY_ID=?  and role=? limit 1',[email,password,company_id,role],(err,results)=>{
     if(results.length>0){
         request.session.role=results[0].role;
         request.session.party_id=results[0].PARTY_ID;
        //  response.render('./login/head2.twig');
          response.redirect('/clientdashboard');
     }
      else{

         response.render('./login/userlogin2.twig',{msg:'Email or Password is wrong',image: request.session.LOGO_LARGE})
     }
  });
});

router.get("/clientdashboard",(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var today_sale = `SELECT IFNULL(sum(net_amount),0) AS TODAY_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND invoice_date=CURRENT_DATE`;
    var today_credit_sale = `SELECT IFNULL(sum(net_amount),0) AS TODAY_CREDIT_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND payment_type='credit' AND invoice_date=CURRENT_DATE`;
    var today_cash_sale = `SELECT IFNULL(sum(net_amount),0) AS TODAY_CASH_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND payment_type='cash' AND invoice_date=CURRENT_DATE`;

    var monthly_sale=`SELECT ROUND(COALESCE(sum(net_amount),0),2) AS Monthly_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND MONTH(invoice_date) = MONTH(CURRENT_DATE())`;
    var monthly_credit_sale=`SELECT ROUND(COALESCE(sum(net_amount),0),2) AS Monthly_credit_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND payment_type='credit'  AND MONTH(invoice_date) = MONTH(CURRENT_DATE())`;
    var monthly_cash_sale=`SELECT ROUND(COALESCE(sum(net_amount),0),2) AS Monthly_cash_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND payment_type='cash'  AND MONTH(invoice_date) = MONTH(CURRENT_DATE())`;


    var total_sale=`SELECT ROUND(sum(net_amount),2) AS TODAY_SALE from sale_master WHERE company_id=${company_id} AND type='S'`;
    var total_credit_sale=`SELECT ROUND(sum(net_amount),2) AS TODAY_credit_SALE from sale_master WHERE company_id=${company_id} AND payment_type='credit'  AND type='S'`;
    var total_cash_sale=`SELECT ROUND(sum(net_amount),2) AS TODAY_cash_SALE from sale_master WHERE company_id=${company_id} AND payment_type='cash'  AND type='S'`;


   var monthly_expense=`SELECT COALESCE(sum(AMOUNT),0) as AMOUNT FROM vouchers WHERE COMPANY_ID=${company_id} and TYPE='EX' AND MONTH(VOUCHER_DATE) = MONTH(CURRENT_DATE())`;
   var total_expense=`SELECT COALESCE(sum(AMOUNT),0) as AMOUNT FROM vouchers WHERE COMPANY_ID=${company_id} and TYPE='EX'`;

   var payables=``;
   
     var remainders=`select remainder.*,parties.name as p_name from remainder LEFT JOIN parties on remainder.PARTY_ID=parties.PARTY_ID WHERE remainder.company_id=${company_id}  AND date=CURRENT_DATE ORDER BY remainder.id DESC`
    var previous_remainders=`select remainder.*,parties.name as p_name from remainder LEFT JOIN parties on remainder.PARTY_ID=parties.PARTY_ID WHERE remainder.company_id=${company_id} AND remainder.status='N' AND date<CURRENT_DATE ORDER BY remainder.id DESC`;


    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(today_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.today_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(today_credit_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.today_credit_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(today_cash_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.today_cash_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(monthly_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.monthly_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(monthly_credit_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.monthly_credit_sale = results;
                parallel_done();
            });
        },

        function(parallel_done) {
            mysqlconnection.query(monthly_cash_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.monthly_cash_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(total_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.total_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(total_credit_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.total_credit_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(total_cash_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.total_cash_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(monthly_expense, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.monthly_expense = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(total_expense, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.total_expense = results;
                parallel_done();
            });
        },
         function(parallel_done) {
            mysqlconnection.query(remainders, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.remainders = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(previous_remainders, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.previouse_remainders = results;
                parallel_done();
            });
        },
    ], function(err) {
        if (err) console.log(err);
//           response.send(return_data.total_expense);
            response.render('./login/clientdashboard.twig',{today_sale:return_data.today_sale , 'today_credit_sale':return_data.today_credit_sale,'today_cash_sale':return_data.today_cash_sale,'monthly_sale':return_data.monthly_sale,'monthly_credit_sale':return_data.monthly_credit_sale,'monthly_cash_sale':return_data.monthly_cash_sale,'total_sale':return_data.total_sale,'total_credit_sale':return_data.total_credit_sale,'total_cash_sale':return_data.total_cash_sale,'monthly_expense':return_data.monthly_expense,'total_expense':return_data.total_expense,'remainders':return_data.remainders,'previous_remainders':return_data.previouse_remainders});
    });


});


module.exports=router;

