/**
 * Created by belawal on 2/25/19.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');


router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
  mysqlconnection.query(`select vouchers.*,parties.NAME as p_name,accounts.NAME as b_name from vouchers LEFT JOIN parties on vouchers.PARTY_ID=parties.PARTY_ID  LEFT JOIN accounts on vouchers.ACCOUNT_ID=accounts.ACCOUNT_ID  where vouchers.COMPANY_ID=${company_id} AND vouchers.type='BDV'`,(err,rows)=>{
      var successmessage = request.flash('cash_payment_success');
      console.log('success message: ' + successmessage);
      response.render('./voucher/bankdeposit/list.twig',{deposits:rows,success : successmessage})
  });

});

router.get('/add',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var parties=`select * from parties where COMPANY_ID=${company_id} AND (type='C' OR type='V')`
    var vouchers=`select * from vouchers where COMPANY_ID=${company_id} AND TYPE='BDV' ORDER BY VOUCHER_ID DESC limit 1`;
    var banks=`select * from accounts where COMPANY_ID=${company_id} AND HEAD_ID=2`
    var return_data = {};

    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(vouchers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.vouchers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(parties, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.parties = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        if(return_data.vouchers.length>0){
            var the_string=return_data.vouchers[0].VOUCHER_NO;
            var parts = the_string.split('-', 2);
            var voucher_no  = parseInt(parts[1])+1;
            voucher_no='BDV-'+voucher_no;
        }
        else{
            voucher_no='BDV-'+1;
        }

       response.render('./voucher/bankdeposit/add.twig',{voucher_no:voucher_no,parties:return_data.parties,banks:return_data.banks});
    });

});

router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    var data={
        VOUCHER_NO:request.body.voucher_no,
        TYPE:'BDV',
        AMOUNT:request.body.amount,
        VOUCHER_DATE:request.body.voucher_date,
        DESCRIPTION:request.body.description,
        MANUAL_NO:request.body.manual_no,
        COMPANY_ID:company_id,
        ACCOUNT_ID:request.body.account_id,
        PARTY_ID:request.body.party_id,
        TIME:request.body.time
    }
    mysqlconnection.query('insert into vouchers set ?', data,(err,rows)=>{
        if(!err) {

            if(request.body.party_id !=""){
                var data={
                    voucher_no:request.body.voucher_no,
                    voucher_date:request.body.voucher_date,
                    type:"BANK_DEPOSIT",
                    account_id:request.body.account_id,
                    party_id:request.body.party_id,
                    credit:request.body.amount,
                    debit:0,
                    COMPANY_ID:company_id,
                    remarks:request.body.description
                }

                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

                var data={
                    voucher_no:request.body.voucher_no,
                    voucher_date:request.body.voucher_date,
                    deposit:request.body.amount,
                    withdraw:0,
                    COMPANY_ID:company_id,
                    account_id:request.body.account_id,
                    remarks:request.body.description
                }

                mysqlconnection.query(`insert into bank_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

            }
            else{
                var data={
                    voucher_no:request.body.voucher_no,
                    voucher_date:request.body.voucher_date,
                    deposit:request.body.amount,
                    account_id:request.body.account_id,
                    withdraw:0,
                    COMPANY_ID:company_id,
                    remarks:request.body.description
                }

                mysqlconnection.query(`insert into bank_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
                var data={
                    voucher_no:request.body.voucher_no,
                    voucher_date:request.body.voucher_date,
                    type:"BANK_DEPOSIT",
                    credit:request.body.amount,
                    COMPANY_ID:company_id,
                    debit:0,
                    remarks:'BANK_DEPOSIT on VOUCHER number '+request.body.voucher_no
                }
                mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }



            request.flash('cash_payment_success',rows.insertId);
            response.redirect('/voucher_bankdeposit/list');
        }
        else{
            console.log(err)
        }
    });
});

router.get('/getlastbalance/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from parties where COMPANY_ID=${company_id} AND Party_ID=${request.params.id}`,(err,rows)=>{
        if(!err){
            var opening_balance=rows[0].OPENING_BALANCE;
            mysqlconnection.query(`SELECT SUM(credit) as credit_sum, SUM(debit) as debit_sum FROM party_ledger WHERE party_id=${request.params.id}`,(err,rows)=>{
                var credit=rows[0].credit_sum;
                var debit=rows[0].debit_sum;
                var last_balance=(opening_balance)-(credit)+(debit);
                response.json(last_balance);
            });
        }
        else{
            response.send(err);
        }
    });
});

router.get('/get_cash_in_hand/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from accounts where COMPANY_ID=${company_id} AND ACCOUNT_ID=${request.params.id}`,(err,rows)=>{
        var opening_balance=rows[0].OPENING_BALANCE;
        console.log("opening balance:"+opening_balance);
        mysqlconnection.query(`SELECT SUM(deposit) as deposit_sum, SUM(withdraw) as withdraw_sum FROM bank_ledger WHERE account_id=${request.params.id} AND COMPANY_ID=${company_id}`,(err,rows)=>{
            var deposit=rows[0].deposit_sum;
            var withdraw=rows[0].withdraw_sum;
            console.log("Deposit :"+deposit + " Withdraw:" +  withdraw);
            var cash_in_hand=(opening_balance)-(withdraw)+(deposit);

            response.json(cash_in_hand);
        });

    });
});



router.get('/edit/:id?',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var record=`select * from vouchers where COMPANY_ID=${company_id} AND VOUCHER_ID=${request.params.id}`;
    var parties=`select * from parties where COMPANY_ID=${company_id} AND (type='C' OR type='V')`;
    var banks=`select * from accounts where COMPANY_ID=${company_id} AND HEAD_ID=2`;
    var return_data = {};

    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(record, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.record = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(parties, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.parties = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        response.render('./voucher/bankdeposit/edit.twig',{record:return_data.record,parties:return_data.parties,banks:return_data.banks});
    });


});

router.post('/update',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
//    mysqlconnection.query(`DELETE FROM vouchers where VOUCHER_ID=${request.body.id}`);
    mysqlconnection.query(`DELETE FROM party_ledger where voucher_no='${request.body.voucher_no}' AND COMPANY_ID=${company_id}`);
    mysqlconnection.query(`DELETE FROM bank_ledger where voucher_no='${request.body.voucher_no}' AND COMPANY_ID=${company_id}`);
    mysqlconnection.query(`DELETE FROM cash_ledger where voucher_no='${request.body.voucher_no}' AND COMPANY_ID=${company_id}`);




    var data={
        VOUCHER_NO:request.body.voucher_no,
        TYPE:'BDV',
        AMOUNT:request.body.amount,
        VOUCHER_DATE:request.body.voucher_date,
        DESCRIPTION:request.body.description,
        MANUAL_NO:request.body.manual_no,
        COMPANY_ID:company_id,
        ACCOUNT_ID:request.body.account_id,
        PARTY_ID:request.body.party_id,
        TIME:request.body.time
    }
    mysqlconnection.query(`update vouchers set ?  where VOUCHER_ID=${request.body.id}`, data,(err,rows)=>{
        if(!err) {

            if(request.body.party_id !=""){
                var data={
                    voucher_no:request.body.voucher_no,
                    voucher_date:request.body.voucher_date,
                    type:"BANK_DEPOSIT",
                    account_id:request.body.account_id,
                    party_id:request.body.party_id,
                    credit:request.body.amount,
                    debit:0,
                    COMPANY_ID:company_id,
                    remarks:request.body.description
                }

                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

                var data={
                    voucher_no:request.body.voucher_no,
                    voucher_date:request.body.voucher_date,
                    deposit:request.body.amount,
                    withdraw:0,
                    COMPANY_ID:company_id,
                    account_id:request.body.account_id,
                    remarks:request.body.description
                }

                mysqlconnection.query(`insert into bank_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

            }
            else{
                var data={
                    voucher_no:request.body.voucher_no,
                    voucher_date:request.body.voucher_date,
                    deposit:request.body.amount,
                    account_id:request.body.account_id,
                    withdraw:0,
                    COMPANY_ID:company_id,
                    remarks:request.body.description
                }

                mysqlconnection.query(`insert into bank_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
                var data={
                    voucher_no:request.body.voucher_no,
                    voucher_date:request.body.voucher_date,
                    type:"BANK_DEPOSIT",
                    credit:request.body.amount,
                    COMPANY_ID:company_id,
                    debit:0,
                    remarks:'BANK_DEPOSIT on VOUCHER number '+request.body.voucher_no
                }
                mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }



            request.flash('cash_payment_success',rows.insertId);
            response.redirect('/voucher_bankdeposit/list');
        }
        else{
            console.log(err)
        }
    });


});

module.exports=router;