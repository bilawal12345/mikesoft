/**
 * Created by belawal on 8/20/19.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select remainder.*,parties.name as p_name from remainder LEFT JOIN parties on remainder.PARTY_ID=parties.PARTY_ID WHERE remainder.company_id=${company_id}  ORDER BY remainder.id DESC`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows)
        var datetime = new Date();

        response.render('./remainder/list.twig',{remainders:rows,current_date:datetime});
    });

});

router.get('/add',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select  * from parties where  company_id=${company_id} AND (type='C' OR type='V')`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./remainder/add.twig',{parties:rows});
    });

});

router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        AMOUNT:request.body.amount,
        DATE:request.body.date,
        DESCRIPTION:request.body.description,
        COMPANY_ID:company_id,
        PARTY_ID:request.body.party_id,
        TIME:request.body.time
    }

    mysqlconnection.query('insert into remainder set ?' , data,(err,rows)=>{
        if(!err){
            response.redirect('/remainder/list');
        }
        else{

        }
    });

});

router.get('/pay/:id?',(req,res)=>{
    var company_id=req.session.COMPANY_ID;
    var data={
        status:'Y'
    }
    mysqlconnection.query(`update remainder set ? Where id=${req.params.id}`, data,(err,rows)=>{
        res.redirect('/clientdashboard')
    });
});

router.get('/unpay/:id?',(req,res)=>{
    var company_id=req.session.COMPANY_ID;
    var data={
        status:'N'
    }
    mysqlconnection.query(`update remainder set ? Where id=${req.params.id}`, data,(err,rows)=>{
        res.redirect('/clientdashboard')
    });
});

module.exports=router;