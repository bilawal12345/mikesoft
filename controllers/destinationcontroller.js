/**
 * Created by belawal on 5/6/19.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');


router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from destinations Where COMPANY_ID=${company_id} ORDER BY id DESC`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./destination/list.twig',{destinations:rows});
    });

});

router.get('/add',(request,response)=>{

    response.render('./destination/add.twig');

});

router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        name:request.body.name,
        COMPANY_ID:company_id
    }
    mysqlconnection.query('INSERT INTO destinations set ?',data,(err,rows)=>{
        if(err) {
            console.log(err)
        }
        else{
            response.redirect('/destination/list');
        }
    })
});

module.exports=router;