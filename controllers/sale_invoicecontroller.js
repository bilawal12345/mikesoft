/**
 * Created by belawal on 2/6/19.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select sale_master.*,parties.name as p_name from sale_master LEFT JOIN parties on sale_master.customer_id=parties.PARTY_ID where sale_master.type='S' AND sale_master.company_id=${company_id} ORDER BY sale_master.ID DESC`,(err,rows)=>{
     if(err) response.send(err);
     response.render('./sale_invoice/list.twig',{sales:rows});
    });

});

router.get('/add',(request,res)=>{
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var categories = `select * from category where COMPANY_ID=${company_id}`;
    var products = `select * from products where COMPANY_ID=${company_id}`;
    var sale_master=`select * from sale_master where type='S' ORDER BY ID DESC limit 1`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(categories, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.invoice = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(products, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.products = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);

        var invoice_number=return_data.invoice[0].invoice_number+1;
        res.render('./sale_invoice/add.twig',{'customers':return_data.customers , 'categories':return_data.categories,'invoice_number':invoice_number,sale_id:sale_id, products:return_data.products,company_id:company_id});
    });
});

router.post('/store',(request,response,cb)=>{
    var company_id=request.session.COMPANY_ID;


   var data={
       invoice_number:request.body.invoice_number,
       invoice_date:request.body.invoice_date,
       payment_type:request.body.payment_type,
       customer_name:request.body.customer_name,
       invoice_time:request.body.time,
       customer_id:request.body.customer_id,
       gross_amount:request.body.gross_amount,
        freight:request.body.freight,
       discount:request.body.discount,
       net_amount:request.body.net_amount,
       internal_notes:request.body.internal_notes,
       external_notes:request.body.external_notes,
       type:'S',
       company_id:company_id
   }
    mysqlconnection.query('insert into sale_master set ?',data,(err,rows)=>{
        if(!err){
            var sale_master_id=rows.insertId;
            var categories=request.body.category_id;
            var product_id=request.body.product_id;
            var type=request.body.type;
             var unit=request.body.unit;
            var quantity=request.body.quantity;
            var price=request.body.price;
            var product_type=request.body.product_type;
            var total=request.body.total;
            var item_discount=request.body.item_discount;
            var cost_price=request.body.cost_price;
            for(var i=0;i<categories.length;i++){

                var data={
                    sale_master_id:sale_master_id,
                    category_id:categories[i],
                    product_id:product_id[i],
                    type:type[i],
                      weight:unit[i],
                    quantity:quantity[i],
                    price:price[i],
                    total:total[i],
                    cost_price:cost_price[i],
                    product_type:product_type[i],
                    item_discount:item_discount[i]
                }
                mysqlconnection.query('insert into sale_child set ?',data,(err,rows)=>{

                });

            }

            for(var i=0;i<categories.length;i++){
                if(product_type[i]=='PRODUCT'){
                    var data={
                        transection_id:request.body.invoice_number,
                        transection_date:request.body.invoice_date,
                        company_id:company_id,
                        category_id:categories[i],
                        product_id:product_id[i],
                        quantity:-(quantity[i]),
                        cost_price:cost_price[i],
                        type:'SALE'
                    }
                    mysqlconnection.query('insert into product_ledger set ?',data,(err,rows)=>{
                        if(err) console.log(err);
                    });
                }

            }


            console.log("the master id is " +rows.insertId);
            request.session.sale_id=rows.insertId;


            if(request.body.payment_type == "credit" && request.body.customer_id != "counter_sale" && request.body.payment == "") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'credit sale'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }


            if (request.body.payment_type == "credit" && request.body.customer_id != "counter_sale" && request.body.payment != "") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'credit sale'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"CRV",
                    party_id:request.body.customer_id,
                    credit:request.body.payment,
                    debit:0,
                    COMPANY_ID:company_id,
                    remarks:'payment received on invoice number'+request.body.invoice_number
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }



            if (request.body.payment_type == "cash" && request.body.customer_id != "counter_sale") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'CASH SALE'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:request.body.net_amount,
                    debit:0,
                    COMPANY_ID:company_id,
                    remarks:'CASH SALE'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }


            if (request.body.payment_type == "cash") {

                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'Cash Sale  on invoice number'+request.body.invoice_number + " on party id " + request.body.customer_id
                }

                mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }


            response.redirect('/sale_invoice/add');
        }
        else{
            response.send(err)
        }
    });
});


router.get('/edit/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var categories = `select * from category where COMPANY_ID=${company_id}`;
    var products = `select * from products where COMPANY_ID=${company_id}`;
    var sale_master=`select * from sale_master where id=${request.params.id}`;
    var sale_child=`select sale_child.* from sale_child WHERE sale_child.sale_master_id=${request.params.id}`;
    var bookers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='B' AND status='Y'`;
    var return_data = {};


    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(categories, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.sale_master = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_child, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.sale_child = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(products, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.products = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(bookers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.bookers = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);

//        var invoice_number=return_data.invoice[0].invoice_number+1;
//             response.send(return_data.sale_child);
        response.render('./sale_invoice/edit.twig',{'customers':return_data.customers , 'categories':return_data.categories,'sale_master':return_data.sale_master,sale_child:return_data.sale_child, products:return_data.products,bookers:return_data.bookers});
    });

});

router.get('/cat_product/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
   mysqlconnection.query(`select * from products where COMPANY_ID=${company_id} AND CATEGORY_ID=${request.params.id}`,(err,rows)=>{
      if(!err){
          response.json(rows);
      }
      else{
          response.send(err);
      }
   });
});

router.get('/print/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
    var sale_master=`select sale_master.*,parties.NAME as party_name,parties.CONTACT_NO,parties.EMAIL,parties.ADDRESS from sale_master LEFT JOIN parties on parties.PARTY_ID=sale_master.customer_id where sale_master.company_id=${company_id} And id=${id}`;
    var sale_child=`select sale_child.*,category.NAME as category_name,products.NAME as product_name from sale_child LEFT JOIN category on category.CATEGORY_ID=sale_child.category_id LEFT JOIN products on products.PRODUCT_ID=sale_child.product_id   where  sale_child.sale_master_id=${id}`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.master = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_child, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.child = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE,
            tax_number:request.session.tax_number
        }
        console.log(return_data.master);
             var party_id=return_data.master[0].customer_id;
        var voucher_number=return_data.master[0].invoice_number;
        console.log(party_id);
        if(party_id==""){
            response.render('./sale_invoice/print4.twig',{'masters':return_data.master , 'childs':return_data.child,session:session_values});
        }
        else{
//            console.log(return_data.master);
            console.log(voucher_number);
            mysqlconnection.query(`select * from parties where COMPANY_ID=${company_id} AND Party_ID=${party_id}`,(err,rows)=>{
                if(!err){
                    var opening_balance=rows[0].OPENING_BALANCE;
                    mysqlconnection.query(`SELECT SUM(credit) as credit_sum, SUM(debit) as debit_sum FROM party_ledger WHERE party_id=${party_id} and voucher_no != ${voucher_number} `,(err,rows)=>{
                 if(err) response.send(err);
//                 response.send(rows);
                        var credit=rows[0].credit_sum;
                        var debit=rows[0].debit_sum;
                        var last_balance=(opening_balance)-(credit)+(debit);
//                        response.json(last_balance);
                        response.render('./sale_invoice/print4.twig',{'masters':return_data.master , 'childs':return_data.child,session:session_values,last_balance:last_balance});
                    });
                }
                else{
                    response.send(err);
                }
            });
        }
        
        // response.render('./sale_invoice/print4.twig',{'masters':return_data.master , 'childs':return_data.child,session:session_values});
    });


});

router.post('/update2',(request,response)=>{

    mysqlconnection.query(`delete from product_ledger where type='SALE' AND transection_id=${request.body.invoice_number}`);
    mysqlconnection.query(`delete from party_ledger where type='SALE' AND voucher_no=${request.body.invoice_number}`);
    mysqlconnection.query(`delete from cash_ledger where type='SALE' AND where voucher_no=${request.body.invoice_number}`);

    mysqlconnection.query(`delete from sale_child where sale_master_id=${request.body.id}`,(err,rows)=>{
        var company_id=request.session.COMPANY_ID;
        var id=request.body.id;

        var data={
            invoice_number:request.body.invoice_number,
            invoice_date:request.body.invoice_date,
            payment_type:request.body.payment_type,
            customer_name:request.body.customer_name,
            invoice_time:request.body.time,
            customer_id:request.body.customer_id,
            gross_amount:request.body.gross_amount,
            discount:request.body.discount,
            net_amount:request.body.net_amount,
            internal_notes:request.body.internal_notes,
            external_notes:request.body.external_notes,
            type:'S',
            company_id:company_id
        }
        mysqlconnection.query(`update sale_master set ? where id=${request.body.id}`,data,(err,rows)=>{
            var sale_master_id=request.body.id;
            var categories=request.body.category_id;
            var product_id=request.body.product_id;
            var type=request.body.type;
            var unit=request.body.unit;
            var quantity=request.body.quantity;
            var price=request.body.price;
            var product_type=request.body.product_type;
            var total=request.body.total;
            var item_discount=request.body.item_discount;
            var cost_price=request.body.cost_price;
            for(var i=0;i<categories.length;i++){

                var data={
                    sale_master_id:sale_master_id,
                    category_id:categories[i],
                    product_id:product_id[i],
                    type:type[i],
                    weight:unit[i],
                    quantity:quantity[i],
                    price:price[i],
                    total:total[i],
                    cost_price:cost_price[i],
                    product_type:product_type[i],
                    item_discount:item_discount[i]
                }
                mysqlconnection.query('insert into sale_child set ?',data,(err,rows)=>{

                });

            }


            for(var i=0;i<categories.length;i++){
                var data={
                    transection_id:request.body.invoice_number,
                    transection_date:request.body.invoice_date,
                    company_id:company_id,
                    category_id:categories[i],
                    product_id:product_id[i],
                    quantity:-(quantity[i]),
                    cost_price:cost_price[i],
                    type:'SALE'
                }
                mysqlconnection.query('insert into product_ledger set ?',data,(err,rows)=>{
                    if(err) console.log(err);
                });
            }


            if(request.body.payment_type == "credit" && request.body.customer_id != "counter_sale" && request.body.payment == "") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'credit sale'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }


            if (request.body.payment_type == "credit" && request.body.customer_id != "counter_sale" && request.body.payment != "") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'credit sale'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"CRV",
                    party_id:request.body.customer_id,
                    credit:request.body.payment,
                    debit:0,
                    COMPANY_ID:company_id,
                    remarks:'payment received on invoice number'+request.body.invoice_number
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }



            if (request.body.payment_type == "cash" && request.body.customer_id != "counter_sale") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'CASH SALE'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:request.body.net_amount,
                    debit:0,
                    COMPANY_ID:company_id,
                    remarks:'CASH SALE'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }


            if (request.body.payment_type == "cash") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'Cash Sale  on invoice number'+request.body.invoice_number + " on party id " + request.body.customer_id
                }
                mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }

            response.redirect('/sale_invoice/list');
        });
    });

});

router.get('/delete/:id?',(request,response)=>{
   var id=request.params.id;
    var invoice_number;
   mysqlconnection.query(`select * from sale_master where id=${id}`,(err,rows)=>{
       invoice_number=rows[0].invoice_number;
       mysqlconnection.query(`DELETE FROM sale_master where id=${id}`);
       mysqlconnection.query(`DELETE FROM sale_child where sale_master_id=${id}`);
       mysqlconnection.query(`Delete from party_ledger where voucher_no=${invoice_number}`);
       response.redirect('/sale_invoice/list');
   });
});

module.exports=router;