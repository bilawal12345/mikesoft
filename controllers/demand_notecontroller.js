/**
 * Created by belawal on 7/10/19.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`select demand_note_master.*,parties.name as p_name from demand_note_master LEFT JOIN parties on demand_note_master.party_id=parties.PARTY_ID where  demand_note_master.company_id=${company_id}`,(err,rows)=>{
        if(err) response.send(err);

            response.render('./demand_note/list.twig',{demands:rows});
    });

//    response.render('./demand_note/list.twig');


});

router.get('/add',(request,res)=>{

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='V'`;
    var categories = `select * from category where COMPANY_ID=${company_id}`;
    var master=`select * from demand_note_master ORDER BY ID DESC limit 1`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(categories, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.invoice = results;
                parallel_done();
            });
        }

    ], function(err) {
        if (err) console.log(err);

        if(return_data.invoice.length>0){
            var demand_number=return_data.invoice[0].demand_number+1;
        }
        else{
            var demand_number=1;
        }

        res.render('./demand_note/add.twig',{'customers':return_data.customers , 'categories':return_data.categories,'demand_number':demand_number});
    });
});

router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        date:request.body.invoice_date,
        demand_number:request.body.demand_number,
        time:request.body.time,
        party_id:request.body.customer_id,
        net_quantity:request.body.net_quantity,
        company_id:company_id
    }
    mysqlconnection.query('insert into demand_note_master set ?',data,(err,rows)=>{
        var purchase_master_id=rows.insertId;
        var categories=request.body.category_id;
        var product_id=request.body.product_id;
        var quantity=request.body.quantity;


        var weight=request.body.weight;

        for(var i=0;i<categories.length;i++){
            var data={
                master_id:purchase_master_id,
                category_id:categories[i],
                product_id:product_id[i],
                quantity:quantity[i],
                weight:weight[i]

            }
            mysqlconnection.query('insert into demand_note_child set ?',data,(err,rows)=>{
                  if(err) console.log(err);
            });
        }
    });
    response.redirect('/demand_note/list');
});

router.get('/print/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
    var sale_master=`select demand_note_master.*,parties.NAME as party_name,parties.CONTACT_NO,parties.EMAIL,parties.ADDRESS from demand_note_master LEFT JOIN parties on parties.PARTY_ID=demand_note_master.party_id where demand_note_master.company_id=${company_id} And id=${id}`;
    var sale_child=`select demand_note_child.*,products.NAME as product_name from demand_note_child  LEFT JOIN products on products.PRODUCT_ID=demand_note_child.product_id   where  demand_note_child.master_id=${id}`;
    var return_data = {};



    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.master = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_child, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.child = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

        response.render('./demand_note/print.twig',{'masters':return_data.master , 'childs':return_data.child,session:session_values});
    });


});


router.get('/get_record_by_demand_number/:number?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT * FROM demand_note_master WHERE demand_number=${request.params.number} And COMPANY_ID=${company_id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});


router.get('/get_child_record_by_demand_number/:number?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT * FROM demand_note_master WHERE demand_number=${request.params.number} And company_id=${company_id}`,(err,rows)=>{
        if(!err){
            if(rows.length>0){
                var id=rows[0].id;

                mysqlconnection.query(`select * from demand_note_child where master_id=${id}`,(err,rows)=>{
                    response.send(rows);
                })

            }
            else{
                response.json('no');
            }

        }
        else{
            response.send(err);
        }
    });
});

module.exports=router;