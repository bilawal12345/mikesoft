/**
 * Created by belawal on 1/28/19.
 */



var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');


var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};
router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');


router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from category where COMPANY_ID = ${company_id}`,(err,rows)=>{
        response.render('./categories/list.twig',{categories:rows});
    })

});

router.get('/add',(request,response)=>{
    response.render('./categories/addcategory.twig');
});

router.get('/delete/:id?',(req,res)=>{
    var id=req.params.id;
    mysqlconnection.query(`Delete from category where CATEGORY_ID=${id}`,(err,rows)=>{
        if(!err){
            console.log('done');
            res.redirect('/category/list')
        }
        else{
            console.log('error'+err)
        }
    });
});

router.get('/edit/:id?',(req,res)=>{
    var id=req.params.id;
    mysqlconnection.query(`SELECT * From category where CATEGORY_ID=${id}`,(err,rows)=>{
        if(!err){
            res.render('./categories/editcategory.twig',{categories:rows})
        }
        else{
            console.log('error'+err)
        }
    });
});

router.post('/store',(request,response)=>{
 var company_id=request.session.COMPANY_ID;
  var data={
   name:request.body.name,
   description:request.body.description,
   COMPANY_ID:company_id
  }
  mysqlconnection.query('INSERT INTO category set ?',data,(err,rows)=>{
      if(err) {
          console.log(err)
      }
      else{
          response.redirect('/category/list');
      }
  })
});

module.exports=router;