/**
 * Created by belawal on 2/4/19.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));




sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');


//expense section//
router.get('/expense',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT * FROM accounts WHERE HEAD_ID='3' and COMPANY_ID=${company_id}`,(err,rows)=>{
        if(!err){
            response.render('./account_head/expense/list.twig',{expenses:rows});
        }
        else{

        }
    });

});

router.get('/expense_add',(request,response)=>{
    response.render('./account_head/expense/add.twig');
});

router.post('/expense_store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        NAME:request.body.name,
        DESCRIPTION:request.body.description,
        HEAD_ID:'3',
        COMPANY_ID:company_id
    }
    mysqlconnection.query('INSERT INTO accounts set ?',data,(err,rows)=>{
        if(!err){
           response.redirect('/account_head/expense');
        }
        else{
            console.log(err);
        }
    });

});

router.get('/expense_edit/:id?',(request,response)=>{
   var  id= request.params.id;
    var company_id=request.session.COMPANY_ID;
   mysqlconnection.query(`SELECT * FROM accounts WHERE COMPANY_ID=${company_id} AND ACCOUNT_ID=${id}`,(err,rows)=>{
       response.render('./account_head/expense/edit_expense.twig',{expenses:rows});
   });

});

router.post('/expense_update',(request,response)=>{
    var  id= request.body.id;
    var company_id=request.session.COMPANY_ID;
    var data={
        NAME:request.body.name,
        DESCRIPTION:request.body.description,
        HEAD_ID:'3',
        COMPANY_ID:company_id
    }
    mysqlconnection.query(`UPDATE accounts set ? where ACCOUNT_ID=${id}`,data,(err,rows)=>{
        response.redirect('/account_head/expense')
    });

});

//end expense

//bank section//
router.get('/bank',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT * FROM accounts WHERE HEAD_ID='2' and COMPANY_ID=${company_id}`,(err,rows)=>{
        if(!err){
            response.render('./account_head/bank/list.twig',{rows:rows});
//            response.send(rows);
        }
        else{

        }
    });

});


router.get('/last_sale_price/:id?/:customer_id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT price,sale_master_id from sale_child WHERE product_id=${request.params.id} AND sale_master_id=(SELECT MAX(id) FROM sale_master WHERE customer_id=${request.params.customer_id} AND company_id=${company_id})`,(err,rows)=>{
        if(rows.length>0){
            var result=rows[0].price;
            response.json(result);
        }
        else{
            result=0
            response.json(result)
        }
    })
})

router.get('/bank_add',(request,response)=>{
    response.render('./account_head/bank/add.twig');
});

router.post('/bank_store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        NAME:request.body.name,
        DESCRIPTION:request.body.description,
        OPENING_BALANCE:request.body.opening_balance,
        HEAD_ID:'2',
        COMPANY_ID:company_id
    }
    mysqlconnection.query('INSERT INTO accounts set ?',data,(err,rows)=>{
        if(!err){
            response.redirect('/account_head/bank');
        }
        else{
            console.log(err);
        }
    });

});

router.get('/bank_edit/:id?',(request,response)=>{
    var  id= request.params.id;
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT * FROM accounts WHERE COMPANY_ID=${company_id} AND ACCOUNT_ID=${id}`,(err,rows)=>{

        response.render('./account_head/bank/edit_bank.twig',{banks:rows});
    });

});

router.post('/bank_update',(request,response)=>{
    var  id= request.body.id;
    var company_id=request.session.COMPANY_ID;
    var data={
        NAME:request.body.name,
        DESCRIPTION:request.body.description,
        OPENING_BALANCE:request.body.opening_balance,
        HEAD_ID:'2',
        COMPANY_ID:company_id
    }
    mysqlconnection.query(`UPDATE accounts set ? where ACCOUNT_ID=${id}`,data,(err,rows)=>{
        response.redirect('/account_head/bank')
    });

});
//end of bank section//


//cash section//


router.get('/cash',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT * FROM accounts WHERE HEAD_ID='1' and COMPANY_ID=${company_id}`,(err,rows)=>{
        if(!err){
            response.render('./account_head/cash/list.twig',{rows:rows});
//            response.send(rows);
        }
        else{

        }
    });

});

router.get('/cash_add',(request,response)=>{
    response.render('./account_head/cash/add.twig');
});


router.post('/cash_store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        NAME:request.body.name,
        DESCRIPTION:request.body.description,
        OPENING_BALANCE:request.body.opening_balance,
        HEAD_ID:'1',
        COMPANY_ID:company_id
    }
    mysqlconnection.query('INSERT INTO accounts set ?',data,(err,rows)=>{
        if(!err){
            response.redirect('/account_head/cash');
        }
        else{
            console.log(err);
        }
    });

});

router.get('/cash_edit/:id?',(request,response)=>{
    var  id= request.params.id;
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT * FROM accounts WHERE COMPANY_ID=${company_id} AND ACCOUNT_ID=${id}`,(err,rows)=>{

        response.render('./account_head/cash/edit_cash.twig',{cashes:rows});
    });

});

router.post('/cash_update',(request,response)=>{
    var  id= request.body.id;
    var company_id=request.session.COMPANY_ID;
    var data={
        NAME:request.body.name,
        DESCRIPTION:request.body.description,
        OPENING_BALANCE:request.body.opening_balance,
        HEAD_ID:'1',
        COMPANY_ID:company_id
    }
    mysqlconnection.query(`UPDATE accounts set ? where ACCOUNT_ID=${id}`,data,(err,rows)=>{
        response.redirect('/account_head/cash')
    });

});

//end of cash//


//income section//

router.get('/income',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT * FROM accounts WHERE HEAD_ID='4' and COMPANY_ID=${company_id}`,(err,rows)=>{
        if(!err){
            response.render('./account_head/income/list.twig',{incomes:rows});
        }
    });

});

router.get('/income_add',(request,response)=>{
    response.render('./account_head/income/add.twig');
});

router.post('/income_store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        NAME:request.body.name,
        DESCRIPTION:request.body.description,
        HEAD_ID:'4',
        COMPANY_ID:company_id
    }
    mysqlconnection.query('INSERT INTO accounts set ?',data,(err,rows)=>{
        if(!err){
            response.redirect('/account_head/income');
        }
        else{
            console.log(err);
        }
    });

});


router.get('/income_edit/:id?',(request,response)=>{
    var  id= request.params.id;
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT * FROM accounts WHERE COMPANY_ID=${company_id} AND ACCOUNT_ID=${id}`,(err,rows)=>{
        response.render('./account_head/income/edit_income.twig',{incomes:rows});
    });

});

router.post('/income_update',(request,response)=>{
    var  id= request.body.id;
    var company_id=request.session.COMPANY_ID;
    var data={
        NAME:request.body.name,
        DESCRIPTION:request.body.description,
        HEAD_ID:'4',
        COMPANY_ID:company_id
    }
    mysqlconnection.query(`UPDATE accounts set ? where ACCOUNT_ID=${id}`,data,(err,rows)=>{
        response.redirect('/account_head/income')
    });

});

//


//for cash in hand//

router.get('/cash_in_hand',(request,response)=>{
    var  id= request.body.id;
    var company_id=request.session.COMPANY_ID;
     mysqlconnection.query(`select * from accounts where HEAD_ID=1 AND COMPANY_ID=${company_id}`,(err,rows)=>{
        if(err) console.log(err)
        if(rows.length>0){
            var opening_balance=rows[0].OPENING_BALANCE;
            mysqlconnection.query(`SELECT SUM(credit) as credit_sum, SUM(debit) as debit_sum FROM cash_ledger WHERE COMPANY_ID=${company_id}`,(err,rows)=>{
               if(rows.length>0){
                   var credit=rows[0].credit_sum;
                   var debit=rows[0].debit_sum;
                   var cash_in_hand=(opening_balance)-(credit)+(debit);
                   response.json(cash_in_hand);
               }
                else{
                   var credit=0;
                   var debit=0;
                   var cash_in_hand=(opening_balance)-(credit)+(debit);
                   response.json(cash_in_hand);
               }

            });
        }
        else{
            var cash_in_hand=0
            response.json(cash_in_hand);
        }

     });
});

// end of cash in hand


//for cash at bank //
router.get('/cash_at_bank',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from accounts where HEAD_ID=2 AND COMPANY_ID=${company_id} ORDER BY ACCOUNT_ID ASC LIMIT 1`,(err,rows)=>{
        if(err) console.log(err);
        if(rows.length>0){
            var opening_balance=rows[0].OPENING_BALANCE;
            console.log(opening_balance);
            var account_id=rows[0].ACCOUNT_ID;
            mysqlconnection.query(`SELECT SUM(deposit) as deposit_sum, SUM(withdraw) as withdraw_sum FROM bank_ledger WHERE COMPANY_ID=${company_id} AND account_id=${account_id}`,(err,rows)=>{
                if(rows.length>0){
                    var deposit_sum=rows[0].deposit_sum;
                    var withdraw_sum=rows[0].withdraw_sum;

                    var cash_at_bank=(opening_balance)-(withdraw_sum)+(deposit_sum);
                    console.log(cash_at_bank);
                    response.json(cash_at_bank);
                }
                else{
                    var deposit_sum=0;
                    var withdraw_sum=0;
                    var cash_at_bank=(opening_balance)-(deposit_sum)+(withdraw_sum);
                    response.json(cash_at_bank);
                }

            });
        }
        else{
            var cash_at_bank=0
            response.json(cash_at_bank);
        }
    });
});
//end of cash at bank//


router.get('/cash_at_bank2',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select ACCOUNT_ID,sum(OPENING_BALANCE) as OPENING_BALANCE from accounts where HEAD_ID=2 AND COMPANY_ID=${company_id} ORDER BY ACCOUNT_ID ASC `,(err,rows)=>{
        if(err) console.log(err);
        if(rows.length>0){
            var opening_balance=rows[0].OPENING_BALANCE;
            var account_id=rows[0].ACCOUNT_ID;

            mysqlconnection.query(`SELECT SUM(deposit) as deposit_sum, SUM(withdraw) as withdraw_sum FROM bank_ledger WHERE COMPANY_ID=${company_id}`,(err,rows)=>{
                if(rows.length>0){
                    var deposit_sum=rows[0].deposit_sum;
                    var withdraw_sum=rows[0].withdraw_sum;

                    var cash_at_bank=(opening_balance)-(withdraw_sum)+(deposit_sum);
                    console.log(cash_at_bank);
                    response.json(cash_at_bank);
                }
                else{
                    var deposit_sum=0;
                    var withdraw_sum=0;
                    var cash_at_bank=(opening_balance)-(deposit_sum)+(withdraw_sum);
                    response.json(cash_at_bank);
                }

            });
        }
        else{
            var cash_at_bank=0
            response.json(cash_at_bank);
        }
    });
});




// today sale//
router.get('/today_sale',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT sum(net_amount) AS TODAY_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND invoice_date=CURRENT_DATE`,(err,rows)=>{
        if(rows.length>0){
           var today_sale=rows[0].TODAY_SALE;
            if(today_sale>0){
                response.json(today_sale);
            }
            else{
                var today_sale=0
                response.json(today_sale);
            }
        }
        else{
            var today_sale=0
            response.json(today_sale);
        }
    })
});
//end of today sale//


//total sale//
router.get('/total_sale',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT ROUND(sum(net_amount),2) AS TODAY_SALE from sale_master WHERE company_id=${company_id} AND type='S'`,(err,rows)=>{
        if(rows.length>0){
            var today_sale=rows[0].TODAY_SALE;
            if(today_sale>0){
                response.json(today_sale);
            }
            else{
                var today_sale=0
                response.json(today_sale);
            }
        }
        else{
            var today_sale=0
            response.json(today_sale);
        }
    })
});
//end of total sale//


// monthly sale//
router.get('/monthly_sale',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT ROUND(COALESCE(sum(net_amount),0),2) AS Monthly_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND MONTH(invoice_date) = MONTH(CURRENT_DATE())`,(err,rows)=>{
        if(rows.length>0){
            var Monthly_SALE =rows[0].Monthly_SALE;
            response.json(Monthly_SALE );
        }
        else{
            var Monthly_SALE =0
            response.json(Monthly_SALE);
        }
    })
});
//end of monthly sale//

//total_expense_ajax//
router.get('/total_expense_ajax',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT COALESCE(sum(AMOUNT),0) as AMOUNT FROM vouchers WHERE COMPANY_ID=${company_id} and TYPE='EX'`,(err,rows)=>{
        if(rows.length>0){
            var AMOUNT =rows[0].AMOUNT;
            response.json(AMOUNT);
        }
        else{
            var AMOUNT =0
            response.json(AMOUNT);
        }
    })
});
//end of total_expense_ajax//

//monthly_expense//
router.get('/monthly_expense',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT COALESCE(sum(AMOUNT),0) as AMOUNT FROM vouchers WHERE COMPANY_ID=${company_id} and TYPE='EX' AND MONTH(VOUCHER_DATE) = MONTH(CURRENT_DATE())`,(err,rows)=>{
        if(rows.length>0){
            var AMOUNT =rows[0].AMOUNT;
            response.json(AMOUNT);
        }
        else{
            var AMOUNT =0
            response.json(AMOUNT);
        }
    })
});
//end of monthly expense//


//today expense//

router.get('/today_expense',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT sum(COALESCE(AMOUNT,0)) as AMOUNT FROM vouchers WHERE COMPANY_ID=${company_id} and TYPE='EX' AND  VOUCHER_DATE=CURRENT_DATE`,(err,rows)=>{

        if(rows.length>0){

            var today_expense=rows[0].AMOUNT;
            if(today_expense>0){
                response.json(today_expense);
            }
            else{
                var today_expense=0
                response.json(today_expense);
            }


        }
        else{
            var AMOUNT =0
            response.json(AMOUNT);
        }
    })
});
//end of today expense//



//receivable//
router.get('/receivables',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT ROUND(sum(credit),0) as credit, ROUND(SUM(debit),2) as debit from party_ledger where party_id in (SELECT party_id from parties where type='C' AND COMPANY_ID=${company_id}) AND company_id=${company_id}`,(err,rows)=>{
        if(rows.length>0){
            var credit=rows[0].credit;
            var debit=rows[0].debit;

         mysqlconnection.query(`SELECT ROUND(SUM(OPENING_BALANCE),2) as OPENING_BALANCE FROM parties WHERE TYPE='C' AND COMPANY_ID=${company_id}`,(err,rows)=>{
            var opening_balance=rows[0].OPENING_BALANCE;

             var receivables=opening_balance-credit + debit;
             if(receivables<0){
                 var receivables =0
                 response.json(receivables);
             }
             else{
                 response.json(receivables);
             }
         });
        }
        else{
            var receivables =0
            response.json(receivables);
        }
    });
});
//end of receivables//


//payable//
router.get('/payables',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT ROUND(IFNULL(sum(credit),0),0) as credit, ROUND(IFNULL(SUM(debit),0),0) as debit from party_ledger where party_id in (SELECT party_id from parties where type='V' AND COMPANY_ID=${company_id}) AND company_id=${company_id}`,(err,rows)=>{
        if(rows.length>0){
            var credit=rows[0].credit;
            var debit=rows[0].debit;
            console.log('after query')


             mysqlconnection.query(`SELECT ROUND(IFNULL(SUM(OPENING_BALANCE),0),0) as OPENING_BALANCE FROM parties WHERE TYPE='V' AND COMPANY_ID=${company_id}`,(err,rows)=>{
                var opening_balance=rows[0].OPENING_BALANCE;

                var payables=opening_balance-credit+debit;
                console.log("the payble is "+payables);
                 response.json(payables);
            });
        }
        else{
            var payables =0
            response.json(payables);
        }
    })
});
//end of receivables//


//reports//
router.get('/report',(request,response)=>{
    response.render('./login/report.twig')
});
//


//cahs payment receipt//
router.get('/receipt_cash_payment',(request,response)=>{
    response.render('./login/receipt_cash_payment.twig')
});
//

//for receipts//
router.get('/voucher_receipt/:id?',(request,response)=>{
   mysqlconnection.query(`select vouchers.*,parties.NAME AS party_name,accounts.Name as account_name from vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID where vouchers.VOUCHER_ID=${request.params.id}`,(err,rows)=>{
       if(err) console.log(err)
       console.log(rows);
        var session_values={
             name:request.session.SHORT_NAME,
             email:request.session.EMAIL,
             contact:request.session.CONTACT,
             logo:request.session.LOGO_LARGE
        }
       response.render('./reports/receipt.twig',{cash_payments:rows,session:session_values})
   })
})
//end of receipts//


//procedure//
router.get('/procedure',(request,response)=>{
    let sql = `CALL PR_ALL_COMAPNY(1)`;

    mysqlconnection.query(sql, true, (error, results, fields) => {
        if (error) {
            return console.error(error.message);
        }
       response.send(results[0]);
    });
})

//end of procedure//

//function//
router.get('/function',(request,response)=>{
    let sql = `CALL HADEEF(81)`;

    mysqlconnection.query(sql, true, (error, results, fields) => {
        if (error) {
            return console.error(error.message);
        }
        response.send(results[0]);
    });
})

//end of function//


//all bank record//

router.get('/all_bank_record',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select  accounts.NAME, accounts.ACCOUNT_ID,accounts.OPENING_BALANCE as OPENING_BALANCE,  sum(COALESCE(bank_ledger.deposit,0)) as deposit,sum(COALESCE(bank_ledger.withdraw,0)) as withdraw  from accounts LEFT JOIN bank_ledger on bank_ledger.account_id=accounts.ACCOUNT_ID where accounts.HEAD_ID=2 AND accounts.COMPANY_ID=${company_id} GROUP BY ACCOUNT_ID`,(err,rows)=>{
        response.render('./account_head/bank/all_record.twig',{rows:rows});
    });
});

//end of all bank record//


//quantity in hand//

router.get('/quantity_in_hand/:id?',(request,response)=>{
   mysqlconnection.query(`SELECT SUM(quantity) as quantity FROM product_ledger WHERE product_id=${request.params.id}`,(err,rows)=>{
       if(rows.length>0){
           var result=rows[0].quantity;
           response.json(result);
       }
       else{
           result=0
           response.json(result)
       }
   })
})

router.get('/get_logo',(request,response)=>{
    response.json(request.session.LOGO_LARGE);
});

router.get('/get_company_name',(request,response)=>{
    response.json(request.session.name);
});

router.get('/change_password',(request,response)=>{
    response.render('./login/change_password.twig')
});

router.post('/change_password',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from parties where COMPANY_ID=${company_id} and role='Admin'`,(err,rows)=>{
       if(request.body.old_password==rows[0].PASSWORD){
          mysqlconnection.query(`update parties set parties.PASSWORD='${request.body.new_password}' where COMPANY_ID=${company_id} and role='Admin'`,(err,rows)=>{
               if(err) console.log(err)
              response.render('./login/change_password.twig',{msg2:'Password has been changed'})
          });
       }
        else{
           response.render('./login/change_password.twig',{msg:'old password is wrong'})
       }
    });
});

module.exports=router;