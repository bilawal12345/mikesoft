/**
 * Created by belawal on 2/21/19.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from vouchers where COMPANY_ID=${company_id} AND TYPE='INC'`,(err,rows)=>{
        var successmessage = request.flash('cash_payment_success');
        console.log('success message: ' + successmessage);
        response.render('./voucher/income/list.twig',{incomes:rows,success : successmessage});
    });
});

router.get('/add',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    var vouchers=`select * from vouchers where COMPANY_ID=${company_id} AND TYPE='INC' ORDER BY VOUCHER_ID DESC limit 1`;
    var incomes=`select * from accounts where COMPANY_ID=${company_id} AND HEAD_ID=4`
    var return_data = {};

    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(vouchers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.vouchers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(incomes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.incomes = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        if(return_data.vouchers.length>0){
            var the_string=return_data.vouchers[0].VOUCHER_NO;
            var parts = the_string.split('-', 2);
            var voucher_no  = parseInt(parts[1])+1;
            voucher_no='INC-'+voucher_no;
        }
        else{
            voucher_no='INC-'+1;
        }

        response.render('./voucher/income/add.twig',{voucher_no:voucher_no,incomes:return_data.incomes});
    });
});

router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    var data={
        VOUCHER_NO:request.body.voucher_no,
        TYPE:'INC',
        AMOUNT:request.body.amount,
        VOUCHER_DATE:request.body.voucher_date,
        DESCRIPTION:request.body.description,
        MANUAL_NO:request.body.manual_no,
        COMPANY_ID:company_id,
        ACCOUNT_ID:request.body.account_id,
        TIME:request.body.time
    }
    mysqlconnection.query('insert into vouchers set ?', data,(err,rows)=>{
        if(!err) {

            var data={
                voucher_no:request.body.voucher_no,
                voucher_date:request.body.voucher_date,
                type:"INCOME",
                credit:0,
                COMPANY_ID:company_id,
                debit:request.body.amount,
                remarks:'INCOME  on VOUCHER number '+request.body.voucher_no
            }
            mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                if(err) console.log(err)

            });
            request.flash('cash_payment_success',rows.insertId);
            response.redirect('/voucher_income/list')
        }
        else{
            console.log(err)
        }
    });
});

router.get('/edit/:id?',(req,res)=>{
    var company_id=req.session.COMPANY_ID;
    var incomes=`select * from accounts where COMPANY_ID=${company_id} AND HEAD_ID=4`;
    var record=`select * from vouchers where VOUCHER_ID=${req.params.id}`;
    var return_data = {};

    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(record, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.record = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(incomes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.incomes = results;
                parallel_done();
            });
        }
    ], function(err) {
//        res.send(return_data.incomes)
        res.render('./voucher/income/edit.twig',{record:return_data.record,incomes:return_data.incomes});
    });
});

router.post('/update',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    var data={
        VOUCHER_NO:request.body.voucher_no,
        TYPE:'INC',
        AMOUNT:request.body.amount,
        VOUCHER_DATE:request.body.voucher_date,
        DESCRIPTION:request.body.description,
        MANUAL_NO:request.body.manual_no,
        COMPANY_ID:company_id,
        ACCOUNT_ID:request.body.account_id,
        TIME:request.body.time
    }

    mysqlconnection.query(`update vouchers set ? Where VOUCHER_ID=${request.body.id}`, data,(err,rows)=>{
        if(err) console.log(err);
        var data={
            voucher_no:request.body.voucher_no,
            voucher_date:request.body.voucher_date,
            type:"INCOME",
            credit:0,
            COMPANY_ID:company_id,
            debit:request.body.amount,
            remarks:'INCOME  on VOUCHER number '+request.body.voucher_no
        }
        mysqlconnection.query(`update cash_ledger set ? where voucher_no='${request.body.voucher_no}' and COMPANY_ID=${company_id}`,data,(err,rows)=>{
            if(err) console.log(err)

        });

        response.redirect('/voucher_income/list')




    });



});

router.get('/delete/:id?',(req,res)=>{
    var company_id=req.session.COMPANY_ID;
    var record=`select * from vouchers where VOUCHER_ID=${req.params.id}`;
    mysqlconnection.query(record,(err,rows)=>{
        if(rows.length>0){
            var voucher_no=rows[0].VOUCHER_NO;
            var type='INCOME';



            mysqlconnection.query(`delete from vouchers where VOUCHER_ID=${req.params.id}`);
            mysqlconnection.query(`delete from cash_ledger where voucher_no='${voucher_no}' and type='INCOME' and COMPANY_ID=${company_id}`,(err,rows)=>{
                if(err) console.log(err);
            });
            res.redirect('/voucher_income/list')
        }

    });
});


module.exports=router;