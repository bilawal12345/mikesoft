

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');


router.get('/Currency', function(req, res){
    res.render('./currency/customerEntryForm.twig');
});

router.get('/NewCurrency', function(req, res){
    res.render('./currency/NewCurrency.twig');
});

// hawal rout path

router.get('/hawala', function(req, res){
	res.render('./currency/hawala.twig');
});





// city route start


router.get('/cities', function (req, res){
    var company_id=req.session.COMPANY_ID;
	mysqlconnection.query(`select * from city where COMPANY_ID=${company_id}`,(err, rows)=>{
		var c = rows
    res.render('./cities/cities.twig',{city:c});
	});
});

router.get('/add', function(req, res){
	res.render('./cities/add.twig');
});

router.post('/add', function(req, res){
    var company_id=req.session.COMPANY_ID;
    var data={
        city:req.body.city,
        COMPANY_ID:company_id
    }
	mysqlconnection.query('insert INTO city set ?',data, function(error, result, fields){
		if(error) throw error;
		else{
			res.redirect('/city/cities');
		}
	});
	});

router.get('/edit/:id?', function(req, res){
	var id = req.params.id;
mysqlconnection.query(`select * from city where id =${id}`,(err,rows)=>{
	if(err) throw err;
	else{
		d =rows;
		res.render('./cities/edit.twig',{data:d});
	}
});
});

router.post('/update/:id?', function(req, res){
	var id = req.params.id;
	mysqlconnection.query(`UPDATE city set ? where id=${id}`,req.body,(err,result,fields)=>{
		if(err) throw err;
		else{
			res.redirect('/city/cities');
		}
	});
});

router.get('/delete/:id?', function(req, res){
var id = req.params.id;
mysqlconnection.query(`DELETE from city where id=${id}`, (err,result)=>{
	if(err) throw err;
	else{
		res.redirect('/city/cities');
	}
});
});
module.exports=router;