/**
 * Created by belawal on 5/26/19.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT purchase_dollar.*,parties.name as p_name FROM purchase_dollar LEFT JOIN parties on purchase_dollar.party_id=parties.PARTY_ID Where purchase_dollar.type='Purchase_dollar'`,(err,rows)=>{
        response.render('./purchase_dollars/list.twig',{purchases:rows});
    });

});

router.get('/add',(request,res)=>{
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var banks=`SELECT * FROM accounts WHERE HEAD_ID='2' and COMPANY_ID=${company_id}`;
    var sale_master=`select * from purchase_dollar ORDER BY ID DESC limit 1`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.invoice = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        var invoice_number
        if(return_data.invoice.length>0){
            invoice_number=return_data.invoice[0].transaction_number+1;
        }
        else{
            invoice_number=1;
        }
//        console.log(invoice_number);



        res.render('./purchase_dollars/add.twig',{'customers':return_data.customers,'banks':return_data.banks,'transaction_number':invoice_number });
    });
});


router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        'transaction_number':request.body.transaction_number,
        date:request.body.date,
        time:request.body.time,
        party_id:request.body.party_id,
        account_id:request.body.account_id,
        party_amount:request.body.party_amount,
        party_rate:request.body.party_rate,
        bank_amount:request.body.bank_amount,
        bank_rate:request.body.bank_rate,
        total_party_amount:request.body.total_party_amount,
        total_bank_amount:request.body.total_bank_amount,
        tax_rate:request.body.tax_rate,
        total_tax_amount:request.body.total_tax_amount,
        per_dollar_rate:request.body.per_dollar_rate,
        type:'Purchase_dollar'
    }
    mysqlconnection.query('INSERT INTO purchase_dollar set ?',data,(err,rows)=>{
        if(err) {
            console.log(err)
        }
        else{
            var data2={
                voucher_no:request.body.transaction_number,
                voucher_date:request.body.date,
                deposit_dollar:request.body.bank_amount,
                withdraw_dollar:0,
                withdraw:0,
                deposit:request.body.total_bank_amount,
                COMPANY_ID:company_id,
                account_id:request.body.account_id

            }


            mysqlconnection.query(`insert into bank_ledger set ?`,data2,(err,rows)=>{
                var data3={
                    voucher_no:request.body.transaction_number,
                    voucher_date:request.body.date,
                    type:"PURCHASE_DOLLAR",
                    party_id:request.body.party_id,
                    credit_dollar:request.body.party_amount,
                    credit:request.body.total_party_amount,
                    debit_dollar:0,
                    debit:0,
                    COMPANY_ID:company_id,
                    remarks:request.body.description
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data3,(err,rows)=>{
                    if(err) console.log(err)

                });

                if(request.body.difference > 0){
                    console.log(' difference is greater ')
                    var data3={
                        voucher_no:request.body.transaction_number,
                        voucher_date:request.body.date,
                        type:"PURCHASE_DOLLAR",
                        party_id:request.body.party_id,
                        credit_dollar:0,
                        credit:0,
                        debit_dollar:0,
                        debit:request.body.difference,
                        COMPANY_ID:company_id,
                        remarks:request.body.description
                    }
                    mysqlconnection.query(`insert into party_ledger set ?`,data3,(err,rows)=>{
                        if(err) console.log(err)

                    });
                }



                response.redirect('/purchase_dollars/list');
            });

        }
    });



});

router.get('/print/:id?',(request,response)=>{
    mysqlconnection.query(`SELECT purchase_dollar.*,parties.name as p_name,accounts.NAME as b_name FROM purchase_dollar  LEFT JOIN accounts on purchase_dollar.account_id=accounts.ACCOUNT_ID LEFT JOIN parties on purchase_dollar.party_id=parties.PARTY_ID where purchase_dollar.id=${request.params.id}`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
        response.render('./purchase_dollars/print.twig',{'purchases':rows,session:session_values});
//       response.send(rows);
    })
});

module.exports=router;