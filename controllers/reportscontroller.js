/**
 * Created by belawal on 3/25/19.
 */
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/all',(request,response)=>{
    var return_data = {};
    var company_id=request.session.COMPANY_ID;
    var banks=`SELECT * FROM accounts WHERE HEAD_ID='2' and COMPANY_ID=${company_id}`;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND (type='C' OR type='V' OR type='B') AND status='Y'`;
    var products=`select * from products where COMPANY_ID=${company_id}`;
    var expenses=`SELECT * FROM accounts WHERE HEAD_ID='3' and COMPANY_ID=${company_id}`
      var cities=`SELECT * FROM city WHERE  COMPANY_ID=${company_id}`
    var categories=`SELECT * FROM category WHERE COMPANY_ID=${company_id}`
    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(products, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.products = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(categories, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        },
          function(parallel_done) {
            mysqlconnection.query(cities, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.cities = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(expenses, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.expenses = results;
                parallel_done();
            });
        }
    ], function(err) {
       if(err) console.log(err);
//         response.send(return_data.categories);
        response.render('./reports/all.twig',{'banks':return_data.banks,'customers':return_data.customers,'products':return_data.products,'expenses':return_data.expenses,'categories':return_data.categories,'cities':return_data.cities});
    });


})



//start of demage report

router.post('/demage_report',(request,response)=>{
    var party_id=request.body.party_id;
    var product_id=request.body.product_id;
    var company_id=request.session.COMPANY_ID;

    var query=`select damages_master.*,products.NAME as product_name,damages_child.id as child_id  from damages_master  LEFT JOIN damages_child on damages_child.damages_master_id=damages_master.id LEFT JOIN products on products.PRODUCT_ID=damages_child.product_id  where  damages_master.company_id=${company_id} AND damages_master.invoice_date >= '${request.body.from_date}' AND damages_master.invoice_date <= '${request.body.to_date}' `;

    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }


//        response.send(rows);
        response.render('./reports/damages.twig',{'damages':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });

});
//end of demage report

router.post('/bank_deposit',(request,response)=>{
       var company_id=request.session.COMPANY_ID;
       mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id} AND vouchers.ACCOUNT_ID=${request.body.account_id} AND vouchers.type='BDV'`,(err,rows)=>{
          if(err) console.log(err);
           var session_values={
               name:request.session.SHORT_NAME,
               email:request.session.EMAIL,
               contact:request.session.CONTACT,
               logo:request.session.LOGO_LARGE
           }

           response.render('./reports/bank_deposit.twig',{'bank_deposits':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
       });
});


router.post('/bank_withdraw',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id} AND vouchers.ACCOUNT_ID=${request.body.account_id} AND vouchers.type='BWV'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

        response.render('./reports/bank_withdraw.twig',{'bank_withdraws':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});



//for categorywise sale report
//for categorywise sale report
router.post('/categorywise_sale_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var party_id=request.body.party_id;
    var category_id=request.body.category_id;

    var query=`Select ifnull(sum(quantity),0) as Total_Qty,sale_child.price,sale_child.total, products.NAME,parties.NAME as party_name from sale_child Left join sale_master on sale_master.id=sale_child.sale_master_id  Left join parties on parties.PARTY_ID=sale_master.booker_id Left join products on products.PRODUCT_ID=sale_child.product_id Where sale_master.invoice_date between '${request.body.from_date}' AND '${request.body.to_date}' And sale_child.category_id=${request.body.category_id} `;

    if(party_id!=0){
        var query2=` AND sale_master.booker_id=${request.body.party_id}`
        query=query+query2;
    }

    query2=` Group by sale_child.product_id `;
    query=query+query2;

//    response.send(query);

    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/categorywise_sale_report.twig',{'sales':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'party_id':party_id});
    });
});
//end of categorywise sale report


router.post('/cash_receive',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id}  AND vouchers.type='CRV'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/cash_receive.twig',{'cash_receives':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});



router.post('/cash_payment',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id}  AND vouchers.type='CPV'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/cash_payment.twig',{'cash_payments':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});


router.post('/income_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id}  AND vouchers.type='INC'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/income_report.twig',{'income_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});



router.post('/price_list_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var category_id=request.body.category_id;
    var query=`select * from products where COMPANY_ID=${company_id} `;
    if(category_id!=0){
        var query2=` AND CATEGORY_ID=${request.body.category_id}`
        query=query+query2;
    }
    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);

        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
        response.render('./reports/price_list_report.twig',{'results':rows,'session':session_values});
    });
});

router.get('/customer_list_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`select * from parties where COMPANY_ID=${company_id} AND type='C' AND status='Y'`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

      response.render('./reports/customer_list_report.twig',{'results':rows,'session':session_values});

    })
});

router.post('/expense_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id}  AND vouchers.type='EX'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/expense_report.twig',{'expense_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});

router.post('/expense_headwise',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var account_id=request.body.account_id;

    var query=`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id}  AND vouchers.type='EX' AND vouchers.ACCOUNT_ID=${account_id}`;
    var headname=`select NAME FROM accounts where ACCOUNT_ID=${account_id}`;



    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(query, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.results = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(headname, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].NAME;

                return_data.headname = x;
                parallel_done();
            });
        },

    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }


        response.render('./reports/expense_headwise.twig',{'complete_reports':return_data.results,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'headname':return_data.headname});
    });



});

router.post('/purchase_return_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select purchase_master.*,parties.name as p_name from purchase_master LEFT JOIN parties on purchase_master.customer_id=parties.PARTY_ID where purchase_master.type='R' AND purchase_master.company_id=${company_id} AND purchase_master.invoice_date >= '${request.body.from_date}' AND purchase_master.invoice_date <= '${request.body.to_date}'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/purchase_return_report.twig',{'returns':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});


router.post('/purchase_invoice_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select purchase_master.*,parties.name as p_name from purchase_master LEFT JOIN parties on purchase_master.customer_id=parties.PARTY_ID where purchase_master.type='P' AND purchase_master.company_id=${company_id} AND purchase_master.invoice_date >= '${request.body.from_date}' AND purchase_master.invoice_date <= '${request.body.to_date}'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/purchase_invoice_report.twig',{'purchase_invoices':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});


router.post('/sale_return_report',(request,response)=>{
    var party_id=request.body.party_id;
    var product_id=request.body.product_id;
    var company_id=request.session.COMPANY_ID;

    var query=`select sale_master.*,parties.name as p_name, sale_child.id as child_id, products.NAME as product_name,GROUP_CONCAT(products.NAME, ' ' , sale_child.price separator '\n') as product_details  from sale_master LEFT JOIN parties on sale_master.customer_id=parties.PARTY_ID LEFT JOIN sale_child on sale_child.sale_master_id=sale_master.id LEFT JOIN products on products.PRODUCT_ID=sale_child.product_id  where sale_master.type='R' AND sale_master.company_id=${company_id} AND sale_master.invoice_date >= '${request.body.from_date}' AND sale_master.invoice_date <= '${request.body.to_date}' `;
    if(party_id!=0){
        var query2=` AND sale_master.customer_id=${request.body.party_id}`
        query=query+query2;
    }
    if(product_id!=0){
        var query2=` AND sale_child.product_id=${request.body.product_id}`
        query=query+query2;
    }
    query2=` GROUP by sale_master.id`;
    query=query+query2;

    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/sale_return_report.twig',{'sale_returns':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});



router.post('/monthly_sale_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`SELECT IFNULL(SUM(net_amount),0) as net_amount,invoice_number,invoice_date from sale_master WHERE sale_master.type='S' AND sale_master.invoice_date BETWEEN '${request.body.from_date}' AND '${request.body.to_date}' AND company_id=${company_id} GROUP BY invoice_number,invoice_date`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/monthly_sale_report.twig',{'sales':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});

router.post('/sale_invoice_report',(request,response)=>{
    var party_id=request.body.party_id;
    var product_id=request.body.product_id;
    var company_id=request.session.COMPANY_ID;

    var query=`select sale_master.*,parties.name as p_name, sale_child.id as child_id, products.NAME as product_name,GROUP_CONCAT(products.NAME, ' ' , sale_child.price, ' * ', sale_child.quantity, ' = ',  sale_child.total   separator '<br>') as product_details  from sale_master LEFT JOIN parties on sale_master.customer_id=parties.PARTY_ID LEFT JOIN sale_child on sale_child.sale_master_id=sale_master.id LEFT JOIN products on products.PRODUCT_ID=sale_child.product_id  where sale_master.type='S' AND sale_master.company_id=${company_id} AND sale_master.invoice_date >= '${request.body.from_date}' AND sale_master.invoice_date <= '${request.body.to_date}' `;
    if(party_id!=0){
        var query2=` AND sale_master.customer_id=${request.body.party_id}`
        query=query+query2;
    }
    if(product_id!=0){
        var query2=` AND sale_child.product_id=${request.body.product_id}`
        query=query+query2;
    }
    query2=` GROUP by sale_master.id`;
    query=query+query2;


    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }


//        response.send(rows);
        response.render('./reports/sale_invoice_report.twig',{'sales':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});



router.post('/expense_complete_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`SELECT vouchers.*,sum(AMOUNT) as ammount,accounts.Name as account_name FROM vouchers LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id} AND vouchers.type='EX' GROUP BY ACCOUNT_ID`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/expense_complete_report.twig',{'expense_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});



//for receivable report//
router.get('/receivable_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT IFNULL(OPENING_BALANCE,0)+IFNULL(view_rec2.debate,0)-IFNULL(view_rec2.credit,0) as BALANCE,NAME FROM parties LEFT JOIN view_rec2 ON view_rec2.party_id=parties.PARTY_ID WHERE parties.COMPANY_ID=${company_id} AND parties.TYPE='C'`,(err,rows)=>{
      var session_values={
          name:request.session.SHORT_NAME,
          email:request.session.EMAIL,
          contact:request.session.CONTACT,
          logo:request.session.LOGO_LARGE
      }
//      response.send(rows)
      response.render('./reports/receivable_report.twig',{'receivable_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

  });
});
//end of reveabile//



router.post('/city_receivable',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT IFNULL(OPENING_BALANCE,0)+IFNULL(view_rec2.debate,0)-IFNULL(view_rec2.credit,0) as BALANCE,NAME FROM parties LEFT JOIN view_rec2 ON view_rec2.party_id=parties.PARTY_ID WHERE parties.COMPANY_ID=${company_id} AND parties.TYPE='C' and parties.city_id=${request.body.city_id}`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/receivable_report.twig',{'receivable_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

    });
});



//for stock report//
router.get('/stock_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT DISTINCT pro.product_id,pro.COST_PRICE,category.CATEGORY_ID,category.NAME as cat_name,pro.NAME,pro.weight,IFNULL(pro.OPENING_BALANCE,0) as OPEN,

(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE' AND product_id=pro.product_id AND company_id=${company_id}) as SALE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as SALE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='DEMAGE' AND product_id=pro.product_id AND company_id=${company_id}) as DEMAGE,
(SELECT IFNULL(SUM(quantity),0)+ IFNULL(OPEN,0) FROM product_ledger WHERE  product_id=pro.product_id AND company_id=${company_id} AND type <> 'OPENING')  as BALANCE
from products as pro
LEFT JOIN category ON
category.CATEGORY_ID=pro.CATEGORY_ID
WHERE pro.company_id=${company_id} ORDER BY category.CATEGORY_ID`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/stock_report.twig',{'stocks':rows,'session':session_values});

    });
});



router.get('/sale_missing_invoice_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT sale_master.*,parties.NAME as p_name FROM sale_master LEFT JOIN parties ON parties.PARTY_ID=sale_master.customer_id WHERE sale_master.company_id=${company_id} AND sale_master.invoice_date >= '2020-02-01' AND sale_master.payment_type='credit' AND sale_master.type='S' and sale_master.invoice_number NOT IN(SELECT voucher_no FROM party_ledger WHERE type='SALE' and company_id=${company_id})`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/sale_missing_invoice_report.twig',{'sales':rows,'session':session_values});

    });

});

router.get('/simple_stock_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT DISTINCT pro.product_id,pro.COST_PRICE,category.CATEGORY_ID,category.NAME as cat_name,pro.NAME,pro.weight,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='OPENING' AND product_id=pro.product_id AND company_id=${company_id}) as OPEN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE' AND product_id=pro.product_id AND company_id=${company_id}) as SALE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as SALE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='DEMAGE' AND product_id=pro.product_id AND company_id=${company_id}) as DEMAGE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE  product_id=pro.product_id AND company_id=${company_id}) as BALANCE
from products as pro
LEFT JOIN category ON
category.CATEGORY_ID=pro.CATEGORY_ID
WHERE pro.company_id=${company_id} ORDER BY category.CATEGORY_ID`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/simple_stock_report.twig',{'stocks':rows,'session':session_values});

    });
});

//end of stock//




//for categorywise profit report//
//for categorywise profit report//
router.post('/categorywise_profit',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    var category_id=request.body.category_id;

    var query=`SELECT products.NAME as product_name, sale_master.invoice_date,sale_master.invoice_number,sale_child.weight,sale_child.product_id,sale_child.quantity,sale_child.cost_price,price,IFNULL(sale_child.quantity,0)*IFNULL(sale_child.cost_price,0) as total_cost,IFNULL(sale_child.quantity,0)*IFNULL(sale_child.price,0) as total_sale_price,IFNULL(sale_child.quantity,0)*IFNULL(sale_child.price,0) - IFNULL(sale_child.quantity,0)*IFNULL(sale_child.cost_price,0) as profit FROM sale_child LEFT JOIN sale_master on sale_master.id = sale_child.sale_master_id LEFT JOIN products on products.PRODUCT_ID = sale_child.product_id WHERE sale_master.invoice_date BETWEEN '${request.body.from_date}' AND '${request.body.to_date}' AND sale_master.company_id=${company_id} AND sale_master.type='S' `;


    if(category_id!=0){
        var query2=` AND sale_child.category_id=${request.body.category_id}`
        query=query+query2;
    }



    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/categorywise_profit.twig',{'sales':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});

//end of categorywise profit report//



//for categorywise stock report//
router.post('/categorywise_stock',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT DISTINCT pro.product_id,pro.COST_PRICE,category.CATEGORY_ID,category.NAME as cat_name,pro.NAME,pro.weight,IFNULL(pro.OPENING_BALANCE,0) as OPEN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE' AND product_id=pro.product_id AND company_id=${company_id}) as SALE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as SALE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='DEMAGE' AND product_id=pro.product_id AND company_id=${company_id}) as DEMAGE,
(SELECT IFNULL(SUM(quantity),0)+OPEN FROM product_ledger WHERE  product_id=pro.product_id AND company_id=${company_id} AND type <> 'OPENING') as BALANCE
from products as pro
LEFT JOIN category ON
category.CATEGORY_ID=pro.CATEGORY_ID
WHERE pro.company_id=${company_id} and category.CATEGORY_ID=${request.body.category_id} ORDER BY category.CATEGORY_ID`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/categorywise_stock_report.twig',{'stocks':rows,'session':session_values});

    });
});
//end of categorywise stock//

//for payablae report//
router.get('/payable_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT IFNULL(OPENING_BALANCE,0)+IFNULL(view_rec2.debate,0)-IFNULL(view_rec2.credit,0) as BALANCE,NAME FROM parties LEFT JOIN view_rec2 ON view_rec2.party_id=parties.PARTY_ID WHERE parties.COMPANY_ID=${company_id} AND parties.TYPE='V'`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/payable_report.twig',{'payable_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

    });
});
//end of reveabile//



router.get('/receivable_report2',(request,response)=>{
    mysqlconnection.query(`SELECT OPENING_BALANCE-view_rec2.credit+view_rec2.debate AS BALANCE,NAME FROM parties LEFT JOIN view_rec2 ON view_rec2.party_id=parties.PARTY_ID WHERE parties.COMPANY_ID=33 AND parties.TYPE='C'`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
        response.send(rows)
//      response.render('./reports/receivable_report.twig',{'receivable_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

    })
});


//profit and loss report//
router.post('/profit_and_loss',(request,response)=>{
    var from_date=request.body.from_date;
    var to_date=request.body.to_date;
    var company_id=request.session.COMPANY_ID;


    var sales=`select IFNULL(sum(net_amount),0) as sales from sale_master WHERE type='S' AND invoice_date BETWEEN '${request.body.from_date}' AND '${request.body.to_date}'  AND company_id=${company_id}`;
    var sale_returns=`select IFNULL(sum(net_amount),0) as returns from sale_master WHERE type='R' AND invoice_date BETWEEN '${request.body.from_date}' AND '${request.body.to_date}'  AND company_id=${company_id}`;
    var expense=`SELECT IFNULL(SUM(AMOUNT),0) as expense FROM vouchers WHERE TYPE='EX' AND VOUCHER_DATE BETWEEN '${request.body.from_date}' AND '${request.body.to_date}' AND company_id=${company_id}`;
    var salecostprice=`select IFNULL(SUM(IFNULL(cost_price,0)*IFNULL(quantity,0)),0) as salecostprice from sale_child LEFT JOIN sale_master ON sale_master.id = sale_child.sale_master_id WHERE sale_master.invoice_date BETWEEN '${request.body.from_date}' AND '${request.body.to_date}'AND company_id=${company_id}   AND sale_master.type='S'`;
    var returncostprice=`select IFNULL(SUM(IFNULL(cost_price,0)*IFNULL(quantity,0)),0) as returncostprice from sale_child LEFT JOIN sale_master ON sale_master.id = sale_child.sale_master_id WHERE sale_master.invoice_date BETWEEN '${request.body.from_date}' AND '${request.body.to_date}'AND company_id=${company_id} AND sale_master.type='R'`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(sales, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].sales;
                return_data.sales = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(salecostprice, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].salecostprice;
                return_data.salecostprice = x;
                parallel_done();
            });
        },

        function(parallel_done) {
            mysqlconnection.query(expense, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].expense;
                return_data.expense = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(returncostprice, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].returncostprice;
                return_data.returncostprice = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_returns, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].returns;
                return_data.returns = x;
                parallel_done();
            });
        }

    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

           console.log(return_data.expense);

        response.render('./reports/profit_and_loss.twig',{'sales':return_data.sales,'returns':return_data.returns,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'expense':return_data.expense,'salecostprice':return_data.salecostprice,'returncostprice':return_data.returncostprice});
    });

});
//end of profit and loss report//


//profit loss report//
router.post('/profit_loss',(request,response)=>{
    var from_date=request.body.from_date;
    var to_date=request.body.to_date;
    var company_id=request.session.COMPANY_ID;


   var sales=`SELECT sum(net_amount) AS sales from sale_master WHERE company_id=${company_id}  AND invoice_date between '${request.body.from_date}' AND '${request.body.to_date}'`;
   var expense=`SELECT IFNULL(SUM(amount),0) as expense from vouchers WHERE company_id=${company_id} and type='EX' AND voucher_date between '${request.body.from_date}' AND '${request.body.to_date}'`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(sales, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].sales;
                return_data.total_sale = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(expense, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].expense;
                return_data.expense = x;
                parallel_done();
            });
        }

    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }



        response.render('./reports/profit_loss.twig',{'total_sale':return_data.total_sale,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'expense':return_data.expense,'opening_balance':return_data.opening_balance});
    });

});
//end of profit loss report//



//for party_leger_report//
router.post('/party_leger_report',(request,response)=>{
    var party_id=request.body.party_id;
    var from_date=request.body.from_date;
    var to_date=request.body.to_date;



    let closing_balance = `SELECT F_CLOSING_BALANCE2(${party_id},'${to_date}') as closing`;
    let opening_balance=`SELECT F_OPENING_BALANCE2(${party_id},'${from_date}') as opening`;
    let complete_reports=`SELECT * FROM party_ledger WHERE voucher_date>='${from_date}' AND voucher_date<='${to_date}' AND party_id=${party_id}`;
    // let complete_reports=`SELECT * FROM party_ledger WHERE voucher_date>='2018-10-23' AND voucher_date<='2019-10-23' AND party_id=681`;
    
    let parties=`SELECT * FROM parties WHERE PARTY_ID=${party_id}`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(closing_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].closing;
                return_data.closing_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(opening_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].opening;
                return_data.opening_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(complete_reports, {}, function(err, results) {
                if (err) return parallel_done(err);

                return_data.complete_reports = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(parties, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.parties = results;
                parallel_done();
            });
        },
    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }


        response.render('./reports/party_ledger_report.twig',{'complete_reports':return_data.complete_reports,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'closing_balance':return_data.closing_balance,'opening_balance':return_data.opening_balance,'parties':return_data.parties});
    });
});

//end of party_leger_report//




//for bank_leger_report//
router.post('/bank_leger_report',(request,response)=>{
    var account_id=request.body.account_id;
    var from_date=request.body.from_date;
    var to_date=request.body.to_date;



    let closing_balance = `SELECT F_BANK_CLOSING2(${account_id},'${to_date}') as closing`;
    let opening_balance=`SELECT F_BANK_OPENING2(${account_id},'${from_date}') as opening`;
    let complete_reports=`SELECT * FROM bank_ledger WHERE voucher_date>='${from_date}' AND voucher_date<='${to_date}' AND account_id=${account_id}`;
    let banks=`SELECT * FROM accounts WHERE ACCOUNT_ID=${account_id}`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(closing_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].closing;
                return_data.closing_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(opening_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].opening;
                return_data.opening_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(complete_reports, {}, function(err, results) {
                if (err) return parallel_done(err);

                return_data.complete_reports = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        }
    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }



        response.render('./reports/bank_ledger_report.twig',{'complete_reports':return_data.complete_reports,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'closing_balance':return_data.closing_balance,'opening_balance':return_data.opening_balance,'banks':return_data.banks});
    });
});

//end of bank_leger_report//

router.get('/try',(request,response)=>{
    var events = [{}];
    var cart_values={
        name:request.body.SHORT_NAME,
        email:request.session.EMAIL,
        contact:request.session.CONTACT,
        logo:request.session.LOGO_LARGE
    }
    events[0][1]=cart_values
    events[0][2]=cart_values

    response.send(events);


})

module.exports=router;