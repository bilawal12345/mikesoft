/**
 * Created by belawal on 1/30/19.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');
sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');




router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select products.*,category.NAME AS CAT_NAME,SUM(quantity) as quantity_in_hand FROM products LEFT JOIN category on category.CATEGORY_ID=products.CATEGORY_ID LEFT JOIN product_ledger on product_ledger.product_id=products.PRODUCT_ID where products.COMPANY_ID = ${company_id} GROUP BY products.PRODUCT_ID`  ,(err,rows)=>{
     if(!err){
             response.render('./product/list.twig',{data:rows})
     }
        else{
         response.send(err);
     }
    })
//    response.render('./product/list.twig')
});

router.get('/add',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from category where COMPANY_ID = ${company_id}`,(err,rows)=>{
        response.render('./product/add.twig',{categories:rows});
    })
});

router.post('/store',upload.single('product_pic'),(request,response)=>{
    var filename="";
    if(!request.file){

    }
    else{
        var file = 'public/products' + '/' + request.file.originalname;
        fs.rename(request.file.path, file, function(err) {
//            response.send('done')
        });
        filename=request.file.originalname;
    }

    var company_id=request.session.COMPANY_ID;

    var data={
        COMPANY_ID:company_id,
        Name:request.body.name,
        product_type:request.body.product_type,
        Description:request.body.description,
        weight:request.body.weight,
        COST_PRICE:request.body.cost_price,
        SALE_PRICE:request.body.sale_price,
        OPENING_BALANCE:request.body.opening_balance,
        IMAGE_NAME:filename,
        CATEGORY_ID:request.body.category_id,
        unit_quantity:request.body.unit_quantity,
        sale_unit:request.body.sale_unit
    }

    mysqlconnection.query('insert INTO products set ?',data,(err,rows)=>{
        if(!err){
            if(request.body.product_type=='SERVICE'){

            }
            else{
                var product_id=rows.insertId;
                var data={
                    product_id:product_id,
                    company_id:company_id,
                    category_id:request.body.category_id,
                    transection_id:product_id,
                    quantity:request.body.opening_balance,
                    cost_price:request.body.cost_price,
                    type:'OPENING'
                }
                mysqlconnection.query('insert INTO product_ledger set ?',data,(err,rows)=>{

                });
            }
           response.redirect('/product/list')
        }
        else{
            response.send(err);
        }
    });
});


router.get('/edit/:id?',(req,res)=>{
    var company_id=req.session.COMPANY_ID;

    var products=`select * from products where COMPANY_ID=${company_id} and PRODUCT_ID=${req.params.id}`;
    var categories=`select * from category where COMPANY_ID = ${company_id}`;
    var return_data = {};

    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(products, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.products = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(categories, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        }
    ], function(err) {

        res.render('./product/edit.twig',{record:return_data.products,categories:return_data.categories});
    });

});

router.post('/update',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var data={
        COMPANY_ID:company_id,
        Name:request.body.name,
        Description:request.body.description,
        weight:request.body.weight,
        COST_PRICE:request.body.cost_price,
        SALE_PRICE:request.body.sale_price,
        OPENING_BALANCE:request.body.opening_balance,
        CATEGORY_ID:request.body.category_id
    }
    mysqlconnection.query(`update products set ? Where PRODUCT_ID=${request.body.id}`, data,(err,rows)=>{
  if(err) console.log(err);

        
        var data={
            product_id:request.body.id,
            company_id:company_id,
            category_id:request.body.category_id,
            transection_id:request.body.id,
            quantity:request.body.opening_balance,
            cost_price:request.body.cost_price,
            type:'OPENING'
        }
   
        mysqlconnection.query(`update product_ledger set ?  Where product_id=${request.body.id}  and  type='OPENING'`,data,(err,rows)=>{
            if(err) console.log(err);
        });

        response.redirect('/product/list')
    });
});


module.exports=router;