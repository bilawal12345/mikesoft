/**
 * Created by belawal on 2/10/20.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');


router.get('/list',(request,response)=>{


    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select  trash.*,parties.NAME as party_name from trash left join parties on parties.PARTY_ID=trash.deleted_by where trash.company_id=${company_id}`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./trash/list.twig',{trashes:rows});
    });

});

module.exports=router;