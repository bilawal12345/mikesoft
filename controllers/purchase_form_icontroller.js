/**
 * Created by belawal on 6/25/19.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`SELECT purchase_dollar.*,parties.name as p_name FROM purchase_dollar LEFT JOIN parties on purchase_dollar.party_id=parties.PARTY_ID Where purchase_dollar.type='FORM'`,(err,rows)=>{

        response.render('./purchase_form_i/list.twig',{sales:rows});
    });

});

router.get('/add',(request,res)=>{
    var sale_id = request.session.sale_id;

    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var sale_master=`select * from purchase_dollar ORDER BY ID DESC limit 1`;
    var products=`select * from products where COMPANY_ID=${company_id}`;
    var banks=`select * from accounts where COMPANY_ID=${company_id} AND HEAD_ID=2`
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.invoice = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(products, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.products = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);

        if(return_data.invoice.length>0){
            var invoice_number=return_data.invoice[0].transaction_number+1;
        }
        else{
            var invoice_number=1;
        }


        res.render('./purchase_form_i/add.twig',{'customers':return_data.customers,'products':return_data.products,'transaction_number':invoice_number,'banks':return_data.banks });
    });
});



router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
       'transaction_number':request.body.transaction_number,
        date:request.body.date,
        time:request.body.time,
        party_id:request.body.party_id,
        account_id:request.body.bank_id,
        form_number:request.body.form_number,
        product_id:request.body.product_id,
        mt:request.body.mt,
        party_amount:request.body.dollar_amount,
        party_rate:request.body.dollar_rate,
        total_party_amount:request.body.total_rate,
        type:'Form'
    }
    mysqlconnection.query('INSERT INTO purchase_dollar set ?',data,(err,rows)=>{
      if(err) console.log(err)

    });

    if(request.body.bank_id==""){
        var data3={
            voucher_no:request.body.transaction_number,
            voucher_date:request.body.date,
            type:"PURCHASE_FORM_I",
            party_id:request.body.party_id,
            credit_dollar:request.body.dollar_amount,
            credit:request.body.total_rate,
            debit_dollar:0,
            debit:0,
            COMPANY_ID:company_id,
            remarks:"Purchase Form I done on Form number " + request.body.form_number
        }

        mysqlconnection.query(`insert into party_ledger set ?`,data3,(err,rows)=>{
            if(err) console.log(err)
            response.redirect('/purchase_form_i/list');
        });
    }
    else{
       var data={
            voucher_no:request.body.transaction_number,
            voucher_date:request.body.date,
            deposit:0,
            COMPANY_ID:company_id,
            withdraw:request.body.total_rate,
            withdraw_dollar:request.body.dollar_amount,
            account_id:request.body.bank_id,
            remarks:" bank Withdraw on form i number "+request.body.form_number
        }

        mysqlconnection.query(`insert into bank_ledger set ?`,data,(err,rows)=>{
            if(err) console.log(err)
            response.redirect('/purchase_form_i/list');
        });
    }


});





router.get('/edit/:id?',(request,response)=>{

    var sale_id = request.session.sale_id;

    request.session.sale_id = null; // resets session variable


    var id=request.params.id;

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;

    var products=`select * from products where COMPANY_ID=${company_id}`;
    var banks=`select * from accounts where COMPANY_ID=${company_id} AND HEAD_ID=2`;
    var master_data=`select * from purchase_dollar where id=${id}`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(products, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.products = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(master_data, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.master_data = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        response.render('./purchase_form_i/edit.twig',{'customers':return_data.customers,'products':return_data.products,'banks':return_data.banks,'master_data':return_data.master_data });
    });

});

router.post('/update',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
   if(request.body.cancel=='yes'){

       if(request.body.bank_id==""){
           var data3={
               voucher_no:request.body.transaction_number,
               voucher_date:request.body.date,
               type:"PURCHASE_FORM_I",
               party_id:request.body.party_id,
               credit_dollar:0,
               credit:0,
               debit_dollar:request.body.dollar_amount,
               debit:request.body.total_rate,
               COMPANY_ID:company_id,
               remarks:"Purchase Form I done on Form number " + request.body.form_number
           }

           mysqlconnection.query(`insert into party_ledger set ?`,data3,(err,rows)=>{
               if(err) console.log(err)
               response.redirect('/purchase_form_i/list');
           });
       }
       else{
           var data={
               voucher_no:request.body.transaction_number,
               voucher_date:request.body.date,
               deposit:request.body.dollar_amount,
               COMPANY_ID:company_id,
               withdraw:0,
               account_id:request.body.bank_id,
               remarks:" bank Withdraw on form i number "+request.body.form_number
           }

           mysqlconnection.query(`insert into bank_ledger set ?`,data,(err,rows)=>{
               if(err) console.log(err)
               response.redirect('/purchase_form_i/list');
           });
       }


   }
    else{
       response.send('no it is not')
   }
})

module.exports=router;