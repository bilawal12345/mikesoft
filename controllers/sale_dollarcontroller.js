/**
 * Created by belawal on 6/14/19.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');


router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT sale_dollar.*,parties.name as p_name,accounts.NAME as b_name FROM sale_dollar LEFT JOIN parties on sale_dollar.party_id=parties.PARTY_ID  LEFT JOIN accounts on sale_dollar.account_id=accounts.ACCOUNT_ID `,(err,rows)=>{
        response.render('./sale_dollars/list.twig',{sales:rows});
//        response.send(rows);
    });

});

router.get('/add',(request,res)=>{
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var banks=`SELECT * FROM accounts WHERE HEAD_ID='2' and COMPANY_ID=${company_id}`;
    var sale_dollars=`select * from sale_dollar ORDER BY id DESC limit 1`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_dollars, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.sale_dollars = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        }

    ], function(err) {
        if (err) console.log(err);

        if(return_data.sale_dollars.length>0){
            var the_form_i_number=return_data.sale_dollars[0].form_i_number+1;

        }
        else{
            the_form_i_number=the_form_i_number+1;
        }
//        console.log(the_form_i_number);
        res.render('./sale_dollars/add.twig',{'customers':return_data.customers,'banks':return_data.banks,'form_i':the_form_i_number });
    });
});

router.get('/get_trip/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT * FROM sale_master where COMPANY_ID=${company_id} AND  type='T'  AND customer_id=${request.params.id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});

router.get('/get_amounts/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT SUM(deposit) as amount_in_pkr, SUM(deposit_dollar) as amount_in_dollar FROM bank_ledger WHERE COMPANY_ID=${company_id} AND account_id=${request.params.id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});

router.get('/get_per_dollar/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT per_dollar_rate FROM purchase_dollar WHERE account_id=${request.params.id} ORDER BY id DESC LIMIT 1`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});


router.post('/store2',(request,response)=>{


});

router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var party_id="";
    var data={
        'form_i_number':request.body.form_i_number,
        date:request.body.date,
        time:request.body.time,
        party_name:request.body.party_name,
        party_id:request.body.party_id,
        trip_id:request.body.trip_id,
        account_id:request.body.account_id,
        amount_in_pkr:request.body.amount_in_pkr,
        amount_in_dollar:request.body.amount_in_dollar,
        amount:request.body.amount,
        per_dollar_rate:request.body.per_dollar_rate,
        type:'SALE_DOLLAR'
    }
    mysqlconnection.query('INSERT INTO sale_dollar set ?',data,(err,rows)=>{
        var manual_number=rows.insertId;


        if(request.body.party_id !=""){
            var data={
                voucher_no:request.body.form_i_number,
                voucher_date:request.body.date,
                type:"SALE_DOLLAR",
                party_id:request.body.party_id,
                credit:0,
                COMPANY_ID:company_id,
                debit:request.body.amount,
                remarks:'it is sale dollar invoice'
            }
            mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                if(err) console.log(err)

            });
        }


        mysqlconnection.query(`select * from vouchers where COMPANY_ID=${company_id} AND TYPE='EX' ORDER BY VOUCHER_ID DESC limit 1`,(err,rows)=>{
            if(rows.length>0){
                var the_string=rows[0].VOUCHER_NO;
                var parts = the_string.split('-', 2);
                var voucher_no  = parseInt(parts[1])+1;
                voucher_no='EXP-'+voucher_no;
            }
            else{
                voucher_no='EXP-'+1;
            }


            var data2={
                VOUCHER_NO:voucher_no,
                TYPE:'EX',
                AMOUNT:request.body.amount,
                VOUCHER_DATE:request.body.date,
                DESCRIPTION:"Sale dollar expense",
                MANUAL_NO:manual_number,
                COMPANY_ID:company_id,
                ACCOUNT_ID:request.body.account_id,
                time:request.body.time
            }
            mysqlconnection.query('insert into vouchers set ?', data2,(err,rows)=>{

            });

        });



        mysqlconnection.query(`select * from vouchers where COMPANY_ID=${company_id} AND TYPE='BWV' ORDER BY VOUCHER_ID DESC limit 1`,(err,rows)=>{
            if(rows.length>0){
                var the_string=rows[0].VOUCHER_NO;
                var parts = the_string.split('-', 2);
                var voucher_no  = parseInt(parts[1])+1;
                voucher_no='BWV-'+voucher_no;
            }
            else{
                voucher_no='BWV-'+1;
            }

            var data2={
                voucher_no:voucher_no,
                voucher_date:request.body.date,
                deposit_dollar:0,
                withdraw_dollar:0,
                withdraw:request.body.amount,
                deposit:0,
                COMPANY_ID:company_id,
                account_id:request.body.account_id,
                remarks:'sale dollar on '+ request.body.form_i_number
            }
            mysqlconnection.query(`insert into bank_ledger set ?`,data2,(err,rows)=>{
                if(err) console.log(err)

            });
        });


        response.redirect('/sale_dollars/list');

    });



});


module.exports=router;