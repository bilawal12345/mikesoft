/**
 * Created by belawal on 8/3/19.
 */
/**
 * Created by belawal on 2/3/19.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from currency where status='Y' AND COMPANY_ID = ${company_id}`,(err,rows)=>{
        response.render('./currencies/list.twig',{currencies:rows});

    })
});

router.get('/add',(request,response)=>{
    response.render('./currencies/add.twig');
});

router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        currency_name:request.body.currency_name,
        currency_sign:request.body.currency_sign,
        COMPANY_ID:company_id
    }
    mysqlconnection.query('INSERT INTO currency set ?',data,(err,rows)=>{
        if(err) {
            console.log(err)
        }
        else{
            response.redirect('/currencies/list');
        }
    })
});

router.get('/edit/:id?',(req,res)=>{
    var id=req.params.id;
    mysqlconnection.query(`SELECT * From currency where currencyid=${id}`,(err,rows)=>{
        if(!err){
            res.render('./currencies/edit.twig',{currencies:rows})
        }
        else{
            console.log('error'+err)
        }
    });
});

router.post('/update',(request,response)=>{


    var company_id=request.session.COMPANY_ID;
    var currencyid=request.body.id;
    var data={
        currency_name:request.body.currency_name,
        currency_sign:request.body.currency_sign,
        COMPANY_ID:company_id
    }
    mysqlconnection.query(`UPDATE currency set ? where currencyid=${currencyid}`,data,(err,rows)=>{
        if(!err){
            response.redirect('/currencies/list');
        }
        else{
            response.send(err);
        }
    });
});


router.get('/delete/:id?',(request,response)=>{


    var id=request.params.id;
    var data={
        status:'N'
    }
    mysqlconnection.query(`UPDATE currency set ? where currencyid=${id}`,data,(err,rows)=>{
        if(!err){
            response.redirect('/currencies/list');
        }
        else{
            response.send(err);
        }
    });
});

module.exports=router;