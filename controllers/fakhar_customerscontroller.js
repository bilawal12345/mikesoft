/**
 * Created by belawal on 8/3/19.
 */
/**
 * Created by belawal on 2/3/19.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`,(err,rows)=>{

        if(!err){
            response.render('./fakhar_customers/list.twig',{customers:rows});
        }
        else{
            console.log(err);
        }
    });


});
router.get('/add',(request,response)=>{

    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`select * from currency where status='Y' AND COMPANY_ID = ${company_id}`,(err,rows)=>{

        if(!err){
            response.render('./fakhar_customers/add.twig',{currencies:rows});
        }
        else{
            console.log(err);
        }
    });
});

router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        NAME:request.body.name,
        Email:request.body.email,
        TYPE:'C',
        CONTACT_NO:request.body.contact_number,
        ADDRESS:request.body.address,
        OPENING_BALANCE:0,
        COMPANY_ID:request.session.COMPANY_ID
    }
    mysqlconnection.query('insert into parties set ?' , data,(err,rows)=>{
        if(!err){
            var party_id=rows.insertId;
            for(var i=0;i<request.body.opening_balance.length;i++){
//                console.log("currency name:" +request.body.currency_name[i] + " Opening Balance: " + request.body.opening_balance[i]+ " currency sign:" +request.body.currency_sign[i]);
                var data={
                    party_id:party_id,
                    currency_id:request.body.currencyid[i],
                    opening_balance:request.body.opening_balance[i]
                }
                mysqlconnection.query('insert into party_opening set ?' , data,(err,rows)=>{
                    if(!err){
                        response.redirect('/fakhar_customers/list');
                    }
                    else{
                        console.log(err)
                    }
                });
                }

        }
        else{

        }
    });

//         for(var i=0; i<rows.length;i++){
//             console.log("Opening is  : "+request.opening_balance[0]);
//         }



});

module.exports=router;
