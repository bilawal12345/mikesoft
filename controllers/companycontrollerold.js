/**
 * Created by belawal on 1/24/19.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var http = require("http");

var multer  = require('multer');
var upload = multer({ dest: 'public/img/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');


var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};
router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get("/add",(request,res)=>{
    console.log(request.session.EMAIL);
    res.render('./company/addcompany.twig');
});

router.post('/registercompany', upload.single('company_logo'), function(request, response) {
    var logo=request.file.originalname;
    var file = 'public/img' + '/' + request.file.originalname;
    fs.rename(request.file.path, file, function(err) {
         console.log('success')
    });
    var company_id;
   var data={
       'NAME':request.body.name,
        'SHORT_NAME':request.body.short_name,
        'DESCRIPTION':request.body.description,
        'CONTACT':request.body.contact,
        'EMAIL':request.body.email,
        'PASSWORD':request.body.password,
         'lat':request.body.lat,
       'lang':request.body.lang,
       'address':request.body.address,
        'LOGO_LARGE':logo,
        'LOGO_SMALL':logo
   }
    mysqlconnection.query('insert INTO company_setup set ? ' ,data,(err,rows)=>{
        if (err) {
            console.log(err);
        }else{
           company_id=rows.insertId;
            var data_party={
                'NAME':request.body.name,
                'DESCRIPTION':request.body.description,
                'CONTACT_NO':request.body.contact,
                'EMAIL':request.body.email,
                'TYPE':'E',
                'role':'Admin',
                'OPENING_BALANCE':0,
                'PASSWORD':request.body.password,
                'COMPANY_ID':company_id
            }

            mysqlconnection.query('insert INTO parties SET ?',data_party,(err,rows)=>{
                if(err){
                    console.log(err)
                }
                else{
                    console.log('success insertion into parties')
                }
            })
        }
        var categories=request.body.check_list;

        for(var i=0;i<categories.length;i++){
            console.log(categories[i])
            var data={
                'name':categories[i],
                'COMPANY_ID':company_id
            }
            mysqlconnection.query('insert into assigned_menus SET ?',data,(err,rows)=>{
                if(err) console.log(err)
            });

        }
    });



    var company=request.body.short_name;
    var mycompany = encodeURI(company);
    var options = {
        host: 'www.hajanaone.com',
        port: 80,
        path: `/api/sendsms.php?apikey=05469878cef2757da06a744036e9adbd&phone=923136258447&sender=SmartSMS&message=${mycompany}+has+been+registered`,
        method: 'GET'
    };

    var req = http.request(options, function(res) {
        console.log('STATUS: ' + res.statusCode);
        console.log('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log('BODY: ' + chunk);
        });
    });

    req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
    });

// write data to request body
    req.write('data\n');
    req.write('data\n');


    var company=request.body.short_name;
    var password=request.body.password;
    var email=request.body.email;
    var contact_no=request.body.contact
    var msg=`Hello your company ${company} has been registered with hissab. Comany Code is ${company}, Your Email is ${email}  Your password is ${password}`;
    var mymsg = encodeURI(msg);
    var options = {
        host: 'www.hajanaone.com',
        port: 80,
        path: `/api/sendsms.php?apikey=05469878cef2757da06a744036e9adbd&phone=${contact_no}&sender=SmartSMS&message=${mymsg}`,
        method: 'GET'
    };

    var req = http.request(options, function(res) {
        console.log('STATUS: ' + res.statusCode);
        console.log('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log('BODY: ' + chunk);
        });
    });
l
    req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
    });

// write data to request body
    req.write('data\n');
    req.write('data\n');

    response.redirect('/company/list');

});

router.get('/list',(request,res)=>{
   mysqlconnection.query('SELECT * From company_setup ORDER BY COMPANY_ID DESC ',(err,rows)=>{
       res.render('./company/companylist.twig',{companies:rows})
   });
});


router.get('/test',(request,res)=>{
//    mysqlconnection.query('SELECT * From company_setup',(err,rows)=>{
//        res.render('./company/test.twig')
//    });
    console.log('hello bilawal the companpy id is:' + request.session.COMPANY_ID);
});

router.get('/head',(request,res)=>{
    res.render('./login/head2.twig')
})

router.post('/test',(request,res)=>{
    var name=request.body.name.length;
    for(var i=0;i<name;i++){
        console.log(request.body.name[i]);
    }
});


router.get('/getcompanies',(request,response)=>{

    mysqlconnection.query(`select * from company_setup order By COMPANY_ID DESC LIMIT 1`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});

router.get('/getcompany/:id?',(request,response)=>{

    mysqlconnection.query(`select * from company_setup where COMPANY_ID=${request.params.id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});

module.exports=router;