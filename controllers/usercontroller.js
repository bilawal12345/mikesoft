/**
 * Created by belawal on 4/11/19.
 */



var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select  * from parties where  type='user' and company_id=${company_id} and (role='operator' or role='manager')`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./users/list.twig',{users:rows});
    });


});

router.get('/add',(request,response)=>{
    response.render('./users/add.twig');
});

router.get('/edit/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select  * from parties where  company_id=${company_id}  and PARTY_ID=${request.params.id}`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./users/edituser.twig',{users:rows});
    });
});


router.post('/store',(request,response)=>{
    var data={
        NAME:request.body.name,
        Email:request.body.email,
        TYPE:'user',
        'PASSWORD':request.body.password,
        role:request.body.role,
        CONTACT_NO:request.body.contact_number,
        ADDRESS:request.body.address,
        DESCRIPTION:request.body.description,
        OPENING_BALANCE:request.body.opening_balance,
        COMPANY_ID:request.session.COMPANY_ID
    }
    mysqlconnection.query('insert into parties set ?' , data,(err,rows)=>{
        if(!err){
            response.redirect('/users/list');
        }
        else{

        }
    });
});


router.post('/update',(request,response)=>{
    var id=request.body.id;
    var data={
        NAME:request.body.name,
        Email:request.body.email,
        TYPE:'user',
        'PASSWORD':request.body.password,
        role:request.body.role,
        CONTACT_NO:request.body.contact_number,
        ADDRESS:request.body.address,
        DESCRIPTION:request.body.description,
        OPENING_BALANCE:request.body.opening_balance,
        COMPANY_ID:request.session.COMPANY_ID
    }
    mysqlconnection.query(`UPDATE parties set ? where PARTY_ID=${id}`,data,(err,rows)=>{
        if(!err){
            response.redirect('/users/list');
        }
        else{
          response.send(err);
        }
    });
});



module.exports=router;