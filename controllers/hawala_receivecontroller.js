/**
 * Created by belawal on 6/18/20.
 */
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`SELECT hawala.*,p1.name as from_customer_name,p2.name as to_customer_name,currency.currency_name FROM hawala LEFT JOIN parties p1 on hawala.from_customer=p1.PARTY_ID LEFT JOIN parties p2 on hawala.to_customer=p2.PARTY_ID LEFT JOIN currency on hawala.currency_id=currency.currencyid where hawala.TYPE='RECEIVE' AND hawala.COMPANY_ID=${company_id} ORDER BY hawala.hawala_id DESC`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./hawala_receive/list.twig',{'messages': request.flash('user'),hawalas:rows});
    });



});


router.get('/add',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var currencies=`select * from currency where status='Y' AND COMPANY_ID = ${company_id}`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(currencies, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.currencies = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);


        response.render('./hawala_receive/add.twig',{'customers':return_data.customers ,'currencies':return_data.currencies ,company_id:company_id});
    });
});


router.post('/store',upload.single('product_pic'),(request,response)=>{
    var filename="";
    if(!request.file){

    }
    else{
        var file = 'public/cnics' + '/' + request.file.originalname;
        fs.rename(request.file.path, file, function(err) {
//            response.send('done')
        });
        filename=request.file.originalname;
    }

    var company_id=request.session.COMPANY_ID;
    var data={
        hawala_number:request.body.hawala_number,
        hawala_date:request.body.hawala_date,
        TYPE:'RECEIVE',
        customer_type:request.body.customer_type,
        from_customer:request.body.from_customer,
        to_customer:request.body.to_customer,
        currency_id:request.body.currency_id,
        hawala_amount:request.body.hawala_amount,
        hawala_commission:request.body.hawala_commission,
        commission_currency_id:request.body.commission_currency_id,
        total_payment:request.body.total_payment,
        remarks:request.body.remarks,
        conversion_currency_id:request.body.conversion_currency_id,
        exchange_rate:request.body.exchange_rate,
        total_amount:request.body.total_amount,
        cnic:filename,
        COMPANY_ID:request.session.COMPANY_ID
    }

    mysqlconnection.query('insert into hawala set ?' , data,(err,rows)=>{
        if(!err){

            var data={
                voucher_no:request.body.hawala_number,
                voucher_date:request.body.hawala_date,
                type:"HAWALA_RECEIVE",
                credit:0,
                COMPANY_ID:company_id,
                debit:request.body.total_payment,
                currency_id:request.body.currency_id,
                remarks:'Hawala Receive  on Hawala number'+request.body.hawala_number
            }

            mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                if(err) console.log(err)

            });

            var data={
                voucher_no:request.body.hawala_number,
                voucher_date:request.body.hawala_date,
                type:"COMMISSION_RECEIVE",
                credit:0,
                COMPANY_ID:company_id,
                debit:request.body.hawala_commission,
                currency_id:request.body.currency_id,
                remarks:'Hawala Commission Receive  on Hawala number'+request.body.hawala_number
            }

            mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                if(err) console.log(err)

            });


            request.flash('user', 'Data Successfully Inserted');
            response.redirect('/hawala_receive/list');
        }
        else{
            console.log('error:'. err)
        }
    });

//         for(var i=0; i<rows.length;i++){
//             console.log("Opening is  : "+request.opening_balance[0]);
//         }



});

router.get('/edit/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var currencies=`select * from currency where status='Y' AND COMPANY_ID = ${company_id}`;

    var hawalas=`select * from hawala where hawala_id=${request.params.id}`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(currencies, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.currencies = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(hawalas, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.hawalas = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);


        response.render('./hawala_receive/edit.twig',{'customers':return_data.customers ,'currencies':return_data.currencies,'hawalas':return_data.hawalas ,company_id:company_id});
    });

});


router.post('/update',upload.single('product_pic'),(request,response)=>{
    var filename="";
    if(!request.file){
        filename=request.body.old_product_pic;
    }
    else{
        var file = 'public/cnics' + '/' + request.file.originalname;
        fs.rename(request.file.path, file, function(err) {
//            response.send('done')
        });
        filename=request.file.originalname;
    }



    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`delete from cash_ledger where voucher_no=${request.body.hawala_number}`,(err,rows)=>{
        var data={
            hawala_number:request.body.hawala_number,
            hawala_date:request.body.hawala_date,
            TYPE:'RECEIVE',
            customer_type:request.body.customer_type,
            from_customer:request.body.from_customer,
            to_customer:request.body.to_customer,
            currency_id:request.body.currency_id,
            hawala_amount:request.body.hawala_amount,
            hawala_commission:request.body.hawala_commission,
            commission_currency_id:request.body.commission_currency_id,
            total_payment:request.body.total_payment,
            remarks:request.body.remarks,
            conversion_currency_id:request.body.conversion_currency_id,
            exchange_rate:request.body.exchange_rate,
            total_amount:request.body.total_amount,
            cnic:filename,
            COMPANY_ID:request.session.COMPANY_ID
        }

        mysqlconnection.query(`update hawala set ? where hawala_id=${request.body.id}`,data,(err,rows)=>{

            if(!err){

                var data={
                    voucher_no:request.body.hawala_number,
                    voucher_date:request.body.hawala_date,
                    type:"HAWALA_RECEIVE",
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.total_payment,
                    currency_id:request.body.currency_id,
                    remarks:'Hawala Receive  on Hawala number'+request.body.hawala_number
                }

                mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

                var data={
                    voucher_no:request.body.hawala_number,
                    voucher_date:request.body.hawala_date,
                    type:"COMMISSION_RECEIVE",
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.hawala_commission,
                    currency_id:request.body.currency_id,
                    remarks:'Hawala Commission Receive  on Hawala number'+request.body.hawala_number
                }

                mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });


                request.flash('user', 'Data Successfully Updated');
                response.redirect('/hawala_receive/list');
            }
            else{
                console.log('error:'. err)
            }
        });
    });
});

router.get('/delete/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
    var invoice_number;
    mysqlconnection.query(`select * from hawala where hawala_id=${id}`,(err,rows)=>{
        invoice_number=rows[0].hawala_number;
        mysqlconnection.query(`delete from hawala where hawala_id=${id} and COMPANY_ID=${company_id}`);
        mysqlconnection.query(`delete from cash_ledger where voucher_no=${invoice_number} and COMPANY_ID=${company_id}`);

    })
    request.flash('user', 'Data Successfully Deleted');
    response.redirect('/hawala_receive/list');
});

module.exports=router;
