/**
 * Created by belawal on 2/2/19.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');


router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`,(err,rows)=>{

        if(!err){
          response.render('./customers/list.twig',{customers:rows});
        }
        else{
            console.log(err);
        }
    });


});

router.get('/add',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from city where COMPANY_ID=${company_id}`,(err, rows)=>{
        response.render('./customers/add.twig',{cities:rows});
    });

});
router.post('/store',(request,response)=>{
   var data={
     NAME:request.body.name,
     Email:request.body.email,
     TYPE:'C',
     CONTACT_NO:request.body.contact_number,
     ADDRESS:request.body.address,
     CITY:request.body.city,
       city_id:request.body.city_id,
     DESCRIPTION:request.body.description,
     OPENING_BALANCE:request.body.opening_balance,
     COMPANY_ID:request.session.COMPANY_ID
   }
   mysqlconnection.query('insert into parties set ?' , data,(err,rows)=>{
       if(!err){
           response.redirect('/customers/add');
       }
       else{

       }
   });
});


router.get('/edit/:id?',(req,res)=>{
    var id=req.params.id;
    mysqlconnection.query(`SELECT * From parties where PARTY_ID=${id}`,(err,rows2)=>{
        if(!err){

            var company_id=req.session.COMPANY_ID;
            mysqlconnection.query(`select * from city where COMPANY_ID=${company_id}`,(err, rows)=>{

                res.render('./customers/editcustomer.twig',{customers:rows2,cities:rows})
            });

        }
        else{
            console.log('error'+err)
        }
    });
});

router.post('/update',(request,response)=>{
    console.log(request.body.party_id);
    var party_id=request.body.party_id;

    var data={
        NAME:request.body.name,
        Email:request.body.email,
        TYPE:'C',
        CONTACT_NO:request.body.contact_number,
        ADDRESS:request.body.address,
          city_id:request.body.city_id,
        DESCRIPTION:request.body.description,
        OPENING_BALANCE:request.body.opening_balance,
        COMPANY_ID:request.session.COMPANY_ID
    }
    mysqlconnection.query(`UPDATE parties set ? where PARTY_ID=${party_id}`,data,(err,rows)=>{
        if(!err){
            response.redirect('/customers/list');
        }
        else{
          response.send(err);
        }
    });
});

router.get('/delete/:id?',(req,res)=>{
    var id=req.params.id;
    mysqlconnection.query(`UPDATE parties SET status='N' WHERE party_id=${id}`,(err,rows)=>{
        if(!err){
            res.redirect('/customers/list');
        }
        else{
            console.log('error'+err)
        }
    });
});




module.exports=router;
